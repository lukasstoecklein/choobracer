﻿using UnityEngine;
using System.Collections;

public enum buttonType { singleplayer, multiplayer, options, quit }

// this class also does the fade out effect
public class ChoobMainMenuButton : MonoBehaviour {

    public buttonType thisButton;
    public Texture offTexture;
    public Texture onTexture;
    private bool buttonPressed = false;

	// Use this for initialization
	void Start () {
        GameObject.Find("MenuCam").GetComponent<Animator>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	   
	}

    void OnMouseDown()
    {
        switch (thisButton)
        {
            case buttonType.singleplayer:
                PlayerPrefs.SetInt("Players", 1);
                GameObject.Find("UniversalControl").GetComponent<Fader>().fadeIntoScene("Choob-SP-Tut");
                break;
            case buttonType.multiplayer:
                PlayerPrefs.SetInt("Players", 2);
                GameObject.Find("UniversalControl").GetComponent<Fader>().fadeIntoScene("Choob-SP-Tut");
                break;
            case buttonType.options:
                GameObject.Find("MenuCam").GetComponent<Animator>().enabled = true;
                break;
            case buttonType.quit:
                GameObject.Find("UniversalControl").GetComponent<Fader>().fadeIntoScene("MainGUI");
                break;
     
        }
    }
    void OnMouseOver()
    {
        this.transform.renderer.material.mainTexture = onTexture;
    }

    void OnMouseExit()
    {
        this.transform.renderer.material.mainTexture = offTexture;
    }
}
