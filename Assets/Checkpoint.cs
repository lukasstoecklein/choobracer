﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {
	private GameObject player;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider c){
		print ("Checkpoint");
		Vector3 direction = c.gameObject.transform.TransformDirection(Vector3.forward);
		//player.GetComponent<SnowPlayerController> ().setLastCheckpointHit (transform);
	}

	void OnDrawGizmosSelected() {
		RaycastHit hit;
		Vector3 direction = transform.TransformDirection(Vector3.forward);
		
		Physics.Raycast (transform.position, direction, out hit);
		Debug.DrawLine (transform.position, hit.point, Color.cyan);
	}
}
