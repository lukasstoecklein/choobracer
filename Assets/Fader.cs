﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {

    private float alphaFadeOutValue = 0;
    private float alphaFadeInValue = 1;
    private bool fadingOut = false;
    private bool fadingIn = false;
    private bool blackOut = false;
    private string sceneTargetName;
    public Texture blackTexture;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {}

    void OnGUI()
    {
        if (blackOut)
        {
            GUI.color = new Color(0, 0, 0, 1);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
        }
        if (fadingOut)
        {
            alphaFadeOutValue += Mathf.Clamp01(Time.deltaTime / 1);

            GUI.color = new Color(0, 0, 0, alphaFadeOutValue);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
            if (alphaFadeOutValue >= 1)
            {
                fadingOut = false;
                alphaFadeOutValue = 0;
                blackOut = true;
                Application.LoadLevel(sceneTargetName);
            }
        }

        if (fadingIn)
        {
            alphaFadeInValue -= Mathf.Clamp01(Time.deltaTime / 1);

            GUI.color = new Color(0, 0, 0, alphaFadeInValue);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture);
            if (alphaFadeInValue <= 0)
            {
                fadingIn = false;
                alphaFadeInValue = 1;
               
            }
        }
    }

    public void fadeIntoScene(string sceneName)
    {
        sceneTargetName = sceneName;
        fadingOut = true;
        blackOut = false;
        
    }

    public void fadeIn()
    {
        blackOut = false;
        fadingIn = true;
    }
}
