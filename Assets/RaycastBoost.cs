﻿using UnityEngine;
using System.Collections;

public class RaycastBoost : MonoBehaviour {

    public GameObject player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        // Raycasts downwards to check if clsoe to blue pad, if so then boost ship
        RaycastHit hit;
        Vector3 offsetZPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 10);
        if (Physics.Raycast(offsetZPosition, -transform.up, out hit, 30))
        {
            // change this to use tags at some point
            if (hit.transform.parent.name == "wall-halfsplit" || hit.transform.parent.name == "wall-full" || hit.transform.parent.name == "wall-halftwist" || hit.transform.parent.name == "wall-animated2" 
                || hit.transform.parent.name == "wall-animated3" || hit.transform.parent.name == "wall-animated4" || hit.transform.parent.name == "wall-animated5" || hit.transform.parent.name == "wall-animated6") 
            {
         
                player.GetComponent<PlayerParentController>().boostShipOnce();
          
            }
            Debug.DrawLine(offsetZPosition, hit.point);

        }
            
        
	}
}
