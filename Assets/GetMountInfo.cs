﻿using UnityEngine;
using System.Collections;

public class GetMountInfo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
    void Update() {
        Quaternion myRot = transform.rotation;
        Quaternion currentHeadingRotation = Quaternion.AngleAxis(myRot.eulerAngles.y, Vector3.up);
        Vector3 posMiddleOfSnowboardBottom = transform.position + (currentHeadingRotation * new Vector3(0.0f, 0.7f, -1.0f));
        Vector3 leftRay = Vector3.left;
        Vector3 rightRay = Vector3.right;


        RaycastHit rcLefvMiddle;
        Physics.Raycast(posMiddleOfSnowboardBottom, leftRay * 5, out rcLefvMiddle);
        Debug.DrawRay(posMiddleOfSnowboardBottom, leftRay * 5, Color.cyan);
        RaycastHit rcRightMiddle;
        Physics.Raycast(posMiddleOfSnowboardBottom, rightRay * 5, out rcRightMiddle);
        Debug.DrawRay(posMiddleOfSnowboardBottom, rightRay * 5, Color.cyan);
	}
}
