﻿using UnityEngine;
using System.Collections;

public class AccuracyController : MonoBehaviour {
	private string exName = "Enter name here";
	private Vector2 scrollPosition;
	public string exerciseTargetsStr = "";
	public LineRenderer lineRenderer;
	private bool texterased = false;
	private bool bShowLine = false;
	private int numTargetTimer = 0;

	private bool playing = false;
	private Vector2[] trails;

	public string[] instructions;
	private int curInstructNum = 0;

	private string pathString = "ATP71 ATP77 ATP7 ATP6 ATP5 ATP4 ATP3 ATP29 ATP12 ATP13 ATP11 ATP10 ATP9 ATP8 ATP19 ATP18 ATP17 ATP16 ATP15 ATP14 ATP27 ATP28 ATP30 ATP31 ATP32 ATP26 ATP25 ATP24 ATP22 ATP23 ATP20 ATP21 ATP75 ATP64 ATP66 ATP69 ATP65 ATP71 ATP46 ATP45 ATP44 ATP43 ATP72 ATP41 ATP34 ATP35 ATP47 ATP73 ATP74 ATP68 ATP70 ATP67 ATP59 ATP60 ATP61 ATP62 ATP63 ATP48 ATP36 ATP33 ATP40 ATP39 ATP37 ATP49 ATP55 ATP56 ATP57 ATP58 ATP52 ATP53 ATP54 ATP50 ATP38 ATP76 ATP52 ATP59 ATP75 ATP19 ATP21 ATP64 ATP67 ATP59 ATP58 ATP60 ATP70 ATP66 ATP20 ATP18 ATP8 ATP7 ATP9 ATP17 ATP23 ATP69 ATP68 ATP61 ATP57 ATP52 ATP53 ATP56 ATP62 ATP74 ATP65 ATP22 ATP16 ATP10 ATP6 ATP77 ATP5 ATP11 ATP15 ATP24 ATP71 ATP73 ATP63 ATP55 ATP54 ATP76 ATP50 ATP49 ATP48 ATP47 ATP46 ATP25 ATP14 ATP13 ATP4 ATP3 ATP12 ATP27 ATP26 ATP45 ATP35 ATP36 ATP37 ATP38 ATP39 ATP33 ATP34 ATP44 ATP32 ATP28 ATP29 ATP30 ATP31 ATP43 ATP41 ATP40 ATP72 ATP43 ATP44 ATP45 ATP46 ATP71 ";

	private float lastTrailSpawnTime = 0.0f;
	private float trailSpawnDistTime = 0.1f;
	private int trailNum = 0;

	public GameObject curTarget;
	public GameObject lastTarget;
	
	private GameObject pointer;
	private GameObject[] targets;

	public float sliderValue = 0.0f;
	private float lastSliderValue = 0.0f;
	private bool hasShownTrail = false;

	public GameObject movingTarget;

	private bool isRunningReplay = false;
	private int replayCounter = 0;
	private float lastReplayIncrement = 0.0f;

	public GUIContent replayBtnContent;
	public GUIStyle replayBtnStyle;

	public  GUIStyle slider, thumb;	//for the horizontal scroller

	// Use this for initialization
	void Start () {
		lineRenderer.GetComponent<LineRenderer>().SetWidth(0.1f,0.1f);	

		pointer = GameObject.FindGameObjectWithTag("Player");
		targets = GameObject.FindGameObjectsWithTag("AccuracyTrackPoint");
		
		trails = new Vector2[500];
		trails[0] = new Vector2(999,999);

		instructions = pathString.Split(' ');

		lastTarget = GameObject.Find ("ATP71");
	}
	
	// Update is called once per frame
	void Update () {
		if(curInstructNum > instructions.Length){
			playing = false;
			showTrail();
		}

		checkForInput ();

		if (playing) {
			moveTarget();
			spawnTrail();
		}

		if (Input.GetKeyDown (KeyCode.Escape))
			Application.LoadLevel (0);
	}

	private void moveTarget(){
		if (atCurTarget ()) {
			getNextTarget ();
			//print ("next target");
		}

		float speed = 2.0f;
		Vector2 curDirection = (curTarget.transform.position - movingTarget.transform.position).normalized * speed * Time.deltaTime;

		/*if (!pointerOnTarget ()) {
				//dont move the moving target
		} 
		else {*/
		//float smooth = 0.5f;
		//Vector2 pos = Vector2.Lerp (movingTarget.transform.position, curTarget.transform.position, Time.deltaTime * smooth);
		Vector2 pos = (Vector2)movingTarget.transform.position + curDirection;
		movingTarget.transform.position = pos;
		//}
	}

	private bool pointerOnTarget(){
		float dist = 0.05f;
		return Vector2.Distance (pointer.transform.position, movingTarget.transform.position) < dist;
	}

	public void addTarget(string s){
		exerciseTargetsStr += s + " ";
		print (exerciseTargetsStr);
	}

	private bool atCurTarget(){
		float dist = 0.05f;
		return curTarget == null || Vector2.Distance (movingTarget.transform.position, curTarget.transform.position) < dist;
	}

	private void getNextTarget(){
		curInstructNum++;
		getTarget ();
	}

	private void getTarget(){
		if(curInstructNum < instructions.Length){	
			string curInstruct = instructions[curInstructNum];
			
			//find target in the list of targets
			foreach(GameObject t in targets){
				if(t.name == curInstruct){
					if(curTarget)
						lastTarget = curTarget;
					curTarget = t;
					
					if(curInstructNum < instructions.Length -1){	//get hover time if it exists
						float hoverTime = 0.0f;
						bool hasHoverTime = float.TryParse(instructions[curInstructNum+1], out hoverTime);	//puts time into timeToHover, if time is in the next 
						if(hasHoverTime && hoverTime > 0){
							curInstructNum++;
						}
					}
				}
			}
			//print (curTarget.name);
		}
	}

	public string getName(){
		//print ("name: " + exName);
		return exName;
	}

	public string getEx(){
		//print ("ex: " + exerciseTargetsStr);
		return exerciseTargetsStr;
	}

	private void startRun(){
		playing = true;
	}

	public void spawnTrail(){
		if(Time.time - lastTrailSpawnTime > trailSpawnDistTime){
			if(trailNum > trails.Length-1)
				increaseTrailsArraySize();

			lastTrailSpawnTime = Time.time;
			trails[trailNum] = pointer.transform.position;
			trailNum++;
		}
	}

	void OnGUI(){		
		if(trails[0].x != 999){
			if(!playing && trails.Length > 0){
				sliderValue = GUI.VerticalSlider(new Rect(Screen.width - 55, Screen.height/2 - 200, 40, 400), (int)sliderValue, 0.0F, trailNum-1, slider, thumb);
				if (GUI.Button(new Rect(Screen.width - 55, Screen.height/2 + 210, 50, 50), replayBtnContent, replayBtnStyle)){
					if(!isRunningReplay){
						isRunningReplay = true;
						lastReplayIncrement = Time.time;
						replayCounter = (int)sliderValue;
					}
					else{	//pause on click if running
						isRunningReplay = false;
					}
				}
			}
		}
	}
	
	private void increaseTrailsArraySize(){
		Vector2[] temp = new Vector2[trails.Length + 100];
		for(int i = 0; i < trails.Length; i++){
			temp[i] = trails[i];
		}
		trails = temp;
	}

	private void checkForInput(){
		if (Input.GetKeyDown (KeyCode.Space)) {
			playing = true;
			trailNum = 0;
			curInstructNum = -1;
			lineRenderer.enabled = false;
		}
	}
	
	
	private void showTrail(){
		lineRenderer.enabled = true;
		lineRenderer.GetComponent<LineRenderer>().SetVertexCount(trailNum);
		for(int i = 0; i < trailNum; i++){
			lineRenderer.GetComponent<LineRenderer>().SetPosition(i, trails[i]);
		}
	}

	public string getCurTargetName(){
		return curTarget.name;
	}
	
	public string getNextTargetName(){
		if(curInstructNum < instructions.Length-1)
			return instructions[curInstructNum+1];
		return curTarget.name;
	}

	public bool getIsPlaying(){
		return playing;
	}
}


