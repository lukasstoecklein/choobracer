﻿using UnityEngine;
using System.Collections;

public class ATPScript : MonoBehaviour {
	private GameObject referee;

	public Color bigCol;
	public Color medCol;

	// Use this for initialization
	void Start () {
		referee = GameObject.Find ("Referee");
		
		bigCol = new Color(20f/255,50f/255f,255f,255);
		medCol = new Color(40f/255f,120f/255f,250f/255f,0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		float normSize = 0.6f;
		float halfSize = 1.1f;
		float bigSize = 2.5f;

		float colorSmooth = 4.0f;

		if (referee.GetComponent<AccuracyController> ().getCurTargetName() == name){
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(bigSize,bigSize,bigSize), Time.deltaTime*8);
			//transform.renderer.material.color = bigCol;
		}
		else if (referee.GetComponent<AccuracyController> ().getNextTargetName() == name){
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(halfSize,halfSize,halfSize), Time.deltaTime*8);
			//transform.renderer.material.color = Color.Lerp (transform.renderer.material.color, medCol, Time.deltaTime * colorSmooth);
		}
		else{
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(normSize,normSize,normSize), Time.deltaTime*8);
			//transform.renderer.material.color = Color.Lerp (transform.renderer.material.color, new Color(255,255,255,0.5f), Time.deltaTime * colorSmooth);
		}

		if (!referee.GetComponent<AccuracyController> ().getIsPlaying ()) {
				transform.renderer.material.color = Color.red;
		} 
		else {
			transform.renderer.material.color = Color.white;
		}
	}

	void OnMouseDown(){
		//referee.GetComponent<AccuracyController> ().addTarget (name);
	}
}
