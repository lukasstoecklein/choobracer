﻿using UnityEngine;
using System.Collections;

enum Positions{
	c,
	iN,iS,iE,iW,iNE,iNW,iSE,iSW,
	oN,oS,oE,oW,oNE,oNW,oSE,oSW
}

public class GameController : MonoBehaviour {
	public GameScore gameScore;
	public Texture2D restart,menu;
	public GUIStyle submitStyle;
	public Texture backBtn;
	private string gameName1 = "Central Balance (10sec)";
	private string gameRun1 = "iN iNW iW iSW iS iSE iE iNE iN";

	private string gameName2 = "Lap (Medium Difficulty)";	
	private string gameRun2 = "c 2 oN oNW oW oSW oS oSE oE oNE oN c iS iSW iW iNW iN iNE iE iSE iS oN 1 oSW 1 oE 1 oNW 1 oS 1";

	private string gameName3 = "Lap (Hard difficulty)";
	private string gameRun3 = "c 0.5 iN iNE iE iSE iS iSW iW iNW iN c 1 oNE iE oSE iS oSW iW oNW iN c 1 iN 0.5 oN 0.5 iN 0.5 c 1 iS 0.5 oS 0.5 is 0.5 c 1 iW 0.5 oW 0.5 iW 0.5 c 1 iE 0.5 oE 0.5 iE 0.5 c 1 iN 0.5 oN 0.5 iN 0.5 c 1 iE 0.5 oE 0.5 iE 0.5 c 1 iS 0.5 oS 0.5 iS 0.5 c 1 iW 0.5 oW 0.5 iW 0.5 c 1";

	private string gameName4 = "Lap (Inner rim clockwise)";
	private string gameRun4 = "c 1 iN iNE iE iSE iS iSW iW iNW iN c 1";	

	private string gameName5 = "Lap (Inner rim counterclockwise)";
	private string gameRun5 = "c 1 iN iNW iW iSW iS iSE iE iNE iN c 1";

	private string testString = "";

	public LineRenderer lineRenderer;
	public GameObject replayCursor;
	private Color c1 = Color.yellow;
	private Color c2 = Color.red;
	private Vector2[] points;

	private string[] instructions;
	private int curInstructNum = 0;
	private string gameName = "Central Balance (10sec)";

	private bool hasHoverTime = false;
	private float hoverTime = 0.0f;
	
	private GameObject[] timers;
	private float[] timesBetweenTargets;
	private Vector2[] trails;
	private GameObject[] targets;
	private GameObject curTarget;

	private GameObject pointer;
	
	public GameObject timerFade;
	public GameObject timerExpander;

	private float startTime = 0.0f; 
	private float lastLapTime = 0.0f;

	private float lastTrailSpawnTime = 0.0f;
	private float trailSpawnDistTime = 0.1f;
	private bool targetHit = false;

	private int numTimesBetweenTargets = 0;
	private float timeAtLastTarget = 0.0f;

	private bool playing = false;
	private bool hasShownTrail = false;
	private bool isRunningReplay = false;

	public float sliderValue = 0.0f;
	private float lastSliderValue = 0.0f;
	private int replayCounter = 0;
	private float lastReplayIncrement = 0.0f;
	public GUIContent replayBtnContent;
	public GUIStyle replayBtnStyle;

	public  GUIStyle slider, thumb;	//for the horizontal scroller
	public GameObject replayCursorTarget;

	private int trailNum = 0;

	
	bool toggleStrictHover = true;

	private bool showTrailWhilePlaying = false;

	private bool pause = false;
	private bool finished = false;
	private bool playerScoreSubmitted = false;
	private string player1Name = "Enter your name";


	// Use this for initialization
	void Start () {

		trails = new Vector2[500];
		trails[0] = new Vector2(999,999);
		timesBetweenTargets = new float[100];
		testString = GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().getCurEx();
		targets = GameObject.FindGameObjectsWithTag("Target");
		timers = GameObject.FindGameObjectsWithTag("Timer");
		pointer = GameObject.FindGameObjectWithTag("Player");
		instructions = testString.Split(' ');
		//getTarget ();
		startTime = Time.time;
		timeAtLastTarget = Time.time;

		lineRenderer.GetComponent<LineRenderer>().SetWidth(0.2f,0.2f);
		print (testString);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && !pause) {
			Screen.showCursor = true;
			print ("pause");
			pause = true;
			//finished = true;
			gameScore.GetComponent<GameScore>().stopTimer();
			
			Time.timeScale = 0;
		}
		else if (Input.GetKeyDown (KeyCode.Escape) && pause) {
			print ("unpause");
			Screen.showCursor = false;
			pause= false;
			gameScore.GetComponent<GameScore>().resumeTimer();
			
			Time.timeScale = 1;
		}
	
		if(!playing)
			checkForSettingsInput();

		if(playing){
			if(curInstructNum > instructions.Length){
				lastLapTime = getLapTime();
				gameScore.finishRace();
				finished = true;
				playing = false;
			}

			if(curTarget)
				if((!toggleStrictHover || curTarget.GetComponent<VisualController>().getIsTriggering()) && (!hasHoverTime || hoverTime <= 0.0f)){
					curTarget.GetComponent<VisualController>().playPulse();
					nextInstruction ();
					recordTimeBetweenTargets();
				}
				else
					curTarget.GetComponent<VisualController>().setCurrent();

			//if hovertime and target is triggering, decrement hovertime left
			if(hasHoverTime && !toggleStrictHover || hasHoverTime && curTarget.GetComponent<VisualController>().getIsTriggering())
				hoverTime -= Time.deltaTime;

			spawnTrail();
			setReplayTargetPos();
		}

		//disable all target highlights
		if(!playing){
			foreach(GameObject t in targets){
				t.GetComponent<VisualController>().deCurrentify();
			}
			replayCursorTarget.SetActive (true);
		}

		//run ended, show the final trail and put into sorted array for replay
		if(!playing && !hasShownTrail){
			showTrail();
		}

		if(!playing && lastSliderValue != sliderValue){ //this triggers if the slider has been moved - so update which trail is showing.
			showTrailAtPoint(Mathf.CeilToInt(sliderValue));
			lastSliderValue = sliderValue;
			replayCursorTarget.GetComponent<ReplayTargetControl>().updateTarget((int)lastSliderValue);
		}

		//active replay
		if(!playing && isRunningReplay){	
			pointerFadedOut(true);
			replayCursor.SetActive (true);
			if(replayCounter > trailNum){
				isRunningReplay = false;
			}
			else{
				//increment
				if(Time.time - lastReplayIncrement > trailSpawnDistTime){
					sliderValue = replayCounter;
					replayCounter++;
					replayCursor.transform.position = Vector2.Lerp(replayCursor.transform.position, (Vector2)trails[replayCounter], Time.deltaTime*5);
				}
			}
		}
		if (!playing && !isRunningReplay) {
			pointerFadedOut(false);
		}
	}

	public void nextInstruction(){
		targetHit = true;
		curInstructNum++;
		if(curTarget)
			replayCursorTarget.GetComponent<ReplayTargetControl> ().setNewPos (trailNum, curTarget.transform.position);
		getTarget();
	}

	void getTarget(){
		if(curInstructNum < instructions.Length){	
			string curInstruct = instructions[curInstructNum];

			if(curTarget)
				curTarget.GetComponent<VisualController>().deCurrentify();

			//find target in the list of targets
			foreach(GameObject t in targets){
				if(t.name == curInstruct){
					curTarget = t;
					t.GetComponent<VisualController>().setCurrent();

					if(curInstructNum < instructions.Length -1){	//get hover time if it exists
						hasHoverTime = float.TryParse(instructions[curInstructNum+1], out hoverTime);	//puts time into timeToHover, if time is in the next 
						if(hasHoverTime && hoverTime > 0){
							curInstructNum++;
							timerFade.GetComponent<TimerFadeController>().setFadeTarget(hoverTime);
							timerExpander.GetComponent<TimerExpanderController>().setHoverTime(hoverTime);
						}
					}
				}
			}
		}
	}

	public void spawnTrail(){
		if(Time.time - lastTrailSpawnTime > trailSpawnDistTime || targetHit){
			if(trailNum > trails.Length-1)
				increaseTrailsArraySize();

			lastTrailSpawnTime = Time.time;
			trails[trailNum] = pointer.transform.position;
			trailNum++;


			if(showTrailWhilePlaying){
				lineRenderer.GetComponent<LineRenderer>().SetVertexCount(trailNum);
				lineRenderer.GetComponent<LineRenderer>().SetPosition(trailNum-1, pointer.transform.position);
			}
		}
	}

	private void setReplayTargetPos(){

	}

	private void showTrail(){
		if(!showTrailWhilePlaying){
			lineRenderer.GetComponent<LineRenderer>().SetVertexCount(trailNum);
			for(int i = 0; i < trailNum; i++){
				lineRenderer.GetComponent<LineRenderer>().SetPosition(i, trails[i]);
			}
		}
	}

	public void showTrailAtPoint(int n){
		replayCursor.SetActive (true);
		replayCursor.transform.position = Vector2.Lerp(replayCursor.transform.position, (Vector2)trails[n], Time.deltaTime*100);
	}

	private void increaseTrailsArraySize(){
		Vector2[] temp = new Vector2[trails.Length + 100];
		for(int i = 0; i < trails.Length; i++){
			temp[i] = trails[i];
		}
		trails = temp;
	}

	public void showTrailAtPointa(int n){

	}

	public float getHoverTime(){
		return hoverTime;
	}

	public string getCurTargetName(){
		if(curTarget)
			return curTarget.name;
		return "No target";
	}

	public float getLapTime(){
		if(playing)
			return Time.time - startTime;
		return getLastLapTimeFinal();
	}

	public float getLastLapTimeFinal(){
		return lastLapTime;
	}

	void OnGUI(){
		if (gameScore.getTime () == 0) {
			GUI.Label (new Rect (20, Screen.height / 8, 150, 50), "Press space to start", submitStyle);
		}
			if (pause) {
			int btnWid = 250;
			int btnHt = 70;
			
			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Time.timeScale = 1;
				Application.LoadLevel ("MainGUI");
			}
		}
		if(finished){
			Screen.showCursor = true;

				
				player1Name = GUI.TextArea(new Rect(Screen.width / 3 , Screen.height/8, 150,50),player1Name,20);	
				if(!playerScoreSubmitted){
				if(GUI.Button (new Rect(Screen.width / 3 + 200,Screen.height/8, 150,50), "Submit score")){
						playerScoreSubmitted = true;
						Score newScore = new Score(player1Name, lastLapTime);
						GameProfile currentGameProfile = GameProfileHolder.instance.getGameProfile();
						if (currentGameProfile.newScore(newScore)){
							
							GUI.Label(new Rect (Screen.width  - 15, 100, 35, 35),"New High Score!"); 
						}
					}
					
				}if(playerScoreSubmitted){
					GUI.Label (new Rect(Screen.width / 2 - (150+ 20),Screen.height/8, 150,50), "Submitted");
				}


			int btnWid = 250;
			int btnHt = 70;
			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Application.LoadLevel (Application.loadedLevel);
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Application.LoadLevel ("MainGUI");
			}
		}
	
		if (GUI.Button (new Rect (10f, Screen.height * (5f/6f), 300f, 150f), backBtn, GUIStyle.none)) {
			Application.LoadLevel("MainMenu");
		}
		if(!playing){
			//toggleStrictHover = GUI.Toggle(new Rect(10, Screen.height - 30, 130, 30), toggleStrictHover, "use this if you need it ok kiddo");
			//GUI.Label(new Rect(10, Screen.height - 100, 300, 100), );
			/*GUI.Label(new Rect(10, Screen.height - 70, 300, 100), "Current game type: \n" + gameName + ".");
			GUI.Label(new Rect(10, Screen.height - 30, 300, 100), "Press space to start!");*/
		}

		if (trails [0].x != 999) {
						if (!playing && trails.Length > 0) {
								sliderValue = GUI.VerticalSlider (new Rect (Screen.width - 55, Screen.height / 2 - 200, 40, 400), (int)sliderValue, 0.0F, trailNum - 1, slider, thumb);
								if (GUI.Button (new Rect (Screen.width - 55, Screen.height / 2 + 210, 50, 50), replayBtnContent, replayBtnStyle)) {
										if (!isRunningReplay) {
												isRunningReplay = true;
												lastReplayIncrement = Time.time;
												replayCounter = (int)sliderValue;
										} else {	//pause on click if running
												isRunningReplay = false;
										}
								}
						}
				}
	}

	void restartLap(){
		gameScore.startTimer ();
		pointerFadedOut(false);

		replayCursor.SetActive(false);
		playing = true;
		hasShownTrail = false;
		curInstructNum = -1;
		nextInstruction ();
		startTime = Time.time;
		trailNum = 0;
		replayCursorTarget.GetComponent<ReplayTargetControl> ().resetAll();
		timesBetweenTargets = new float[100];

		if(!showTrailWhilePlaying)
			lineRenderer.SetVertexCount(0);

		//destroy last trail
		foreach(Vector2 t in trails)
			t.Set(0,0);
	}

	private void pointerFadedOut(bool b){
		float fadeAmount = 1.0f;
		if(b)fadeAmount = 0.1f;
		Color oldColor = pointer.renderer.material.color;
		Color newColor = new Color(oldColor.r, oldColor.b, oldColor.g, fadeAmount);          
		pointer.renderer.material.SetColor("_Color", newColor); 
	}

	private void recordTimeBetweenTargets(){
		timesBetweenTargets[numTimesBetweenTargets] = Time.time - timeAtLastTarget;
		timeAtLastTarget = Time.time;
		numTimesBetweenTargets++;
	}

	void checkForSettingsInput(){
		if(Input.GetKeyUp("space")) //starting a new lap when the last one finishes
			restartLap();

		if(Input.GetKeyDown(KeyCode.Alpha1)){ //starting a new lap when the last one finishes
			testString = gameRun1;
			instructions = testString.Split(' ');
			gameName = gameName1;
			toggleStrictHover = false;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha2)){ //starting a new lap when the last one finishes
			testString = gameRun2;
			instructions = testString.Split(' ');
			gameName = gameName2;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha3)){ //starting a new lap when the last one finishes
			testString = gameRun3;
			instructions = testString.Split(' ');
			gameName = gameName3;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha4)){ //starting a new lap when the last one finishes
			testString = gameRun4;
			instructions = testString.Split(' ');
			gameName = gameName4;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha5)){ //starting a new lap when the last one finishes
			testString = gameRun5;
			instructions = testString.Split(' ');
			gameName = gameName5;
			toggleStrictHover = true;
		}
	}
}

	//only deprecate timer on targets that you have to hover over when the icon is close enough, if theyve been over it for 0.5 secs

