using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class MovementController : MonoBehaviour {
	private float speed = 20.0f;
	public GameObject udp;

	public bool simulateData = false;
	public bool simulateUDP = false;

	//all these get calculated later
	private float moveX = 0.0f;
	private float moveY = 0.0f;

	private SerialPort sp;
	private string[] bits;

	public GameObject board;
	private Vector3 center3 = new Vector3(0,0,0);

	// Use this for initialization
	void Start () {
		if(!simulateData){
			sp = new SerialPort("COM7", 115200);
			sp.Open();
			sp.ReadTimeout = 1; //apparently if this is set higher, unity might freeze while its trying to read
		}
	}

	void FixedUpdate(){
		if(simulateData && !simulateUDP)
			mouseMove();
		else 
			gyroMove();

		constrainToCircle();
	}
	
	// Update is called once per frame
	void Update () {
		if(simulateData){
			if(udp && simulateUDP){
				double[] axis = udp.GetComponent<Mindtuner>().getOrientationVector();
				
				if(axis != null)
				{
					moveX = -(float)axis[1];
					moveY = (float)axis[0];
				}
				print("MoveX: " + moveX + "\tMoveY: " + moveY);
				
			}
			else{
				moveX = Input.GetAxis("Horizontal");
				moveY = Input.GetAxis("Vertical");
			}
		}
		else{
			//need to make sure the port is seriously open, otherwise catch the exception.
			if(sp.IsOpen){
				try{
					string lastValidLine = "";
					string lastLine = "";

					while ((lastLine = sp.ReadLine()) != null) {
						if(lastLine[0] == '(' && lastLine.Contains ("\t")){
							lastValidLine = lastLine;

							bits = lastValidLine.Split('\t');

							float pitch;
							float.TryParse(bits[1], out pitch);
							//	print("pitch "+pitch);
							
							float roll;
							float.TryParse(bits[2], out roll);
							//print (Mathf.Abs (pitch - moveX) + " " +  Mathf.Abs (roll - moveY));

							moveX = pitch;
							moveY = roll;
						}
					}
					//print (lastlastValidLine);
					//print(lastValidLine);
				}
				catch(System.Exception){} //just catch it, dont freak 
			}
		}
	}

	private void mouseMove(){
		transform.position = Vector2.Lerp (transform.position, new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Time.deltaTime*12);
	}

	private void gyroMove(){
		float gyroMax = 0.2f;
		float boardMax = 4.7f;

		float targetX = moveX/gyroMax*boardMax;
		float targetY = moveY/gyroMax*boardMax;

		float smooth = 8.0f;

		transform.position = Vector2.Lerp(transform.position, new Vector2(targetX,targetY), Time.deltaTime * smooth);
	}

	private void constrainToCircle(){
		float boardMax = 4.7f;
		if(Vector2.Distance(transform.position, center3) > boardMax){
			Vector2 newPos = transform.position - center3;
			newPos.Normalize();
			newPos *= boardMax;
			transform.position = newPos;
			Screen.showCursor = true;
		}
		else
			Screen.showCursor = false;
	}
}
