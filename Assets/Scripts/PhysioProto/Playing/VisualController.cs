﻿using UnityEngine;
using System.Collections;

public class VisualController : MonoBehaviour {
	private float deltaTime = 0.0f;
	private float fadeTime = 0.05f;
	private float fadeVal = 1.0f;

	private float fadeInSpeed = 0.3f;
	private float fadeOutSpeed = 0.1f;

	private bool isFadingIn = false;
	private bool isFadingOut = false;
	private bool pulseFadeIn = true;
	private bool pulseFadeOut = false;
	private bool isNext = false;
	private bool isCurrent = false;
	private bool isTriggering = false;

	private GameObject referee;

	// Use this for initialization
	void Start () {
		Color c = renderer.material.color;
		c.a = 0.0f;
		renderer.material.color = c;

		referee = GameObject.FindGameObjectWithTag("Referee");

		//animation["pulse"].speed = 1.5f;
		//animation["pulse"].wrapMode = WrapMode.Clamp;
	}
	
	// Update is called once per frame
	void Update () {		
		setColor();

		if(isNext)
			pulse ();
		else if(isTriggering && !isCurrent)
			FadeOut (0.2f);
		else if(isCurrent)
			FadeIn(1.0f);
		else if(isTriggering)
			FadeIn (0.2f);
		else 
			FadeOut(0.2f);
	}

	void FadeIn(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal < amount){
				fadeVal += fadeInSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}
	
	void FadeOut(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal > amount){
				fadeVal -= fadeOutSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}

	void setColor(){		
		if(referee.GetComponent<GameController>().getCurTargetName() == gameObject.name && referee.GetComponent<GameController>().getHoverTime() > 0.0f)
			renderer.material.color = Color.gray;
		else
			renderer.material.color = Color.white;

		Color c = renderer.material.color;
		c.a = fadeVal;
		renderer.material.color = c;
	}
	
	void pulse(){
		if(fadeVal >= 1.0f){
			print ("fading out");
			pulseFadeIn = true;
		}
		if(fadeVal <= 0.0f){
			print ("fading in");
			pulseFadeIn = false;
		}

		if(pulseFadeIn)
			FadeIn (1.0f);
		else
			FadeOut (0.0f);
	}

	public void setNext(){
		isNext = true;
		isCurrent = false;
	}
	
	public void setCurrent(){
		isCurrent = true;
		isNext = false;
	}

	public void deCurrentify(){
		isCurrent = false;
		isNext = false;
	}
	
	void OnTriggerEnter2D(Collider2D col){
		isTriggering = true;
	}

	void OnTriggerExit2D(Collider2D col){
		isTriggering = false;
	}
	
	public bool getIsTriggering(){
		return isTriggering;
	}

	public void playPulse(){
		//animation.Play("pulse");
	}
}
