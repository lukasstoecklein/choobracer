﻿using UnityEngine;
using System.Collections;

public class TiltWithInput : MonoBehaviour {
	float x = 0.0f;
	float y = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float tgtRotX = 150.0f*x;
		float tgtRotY = 150.0f*y;
		
		float targetRotationX = 360.0f+tgtRotX;
		float targetRotationY = 360.0f+tgtRotY;
		transform.localEulerAngles = new Vector3(targetRotationX,targetRotationY,transform.localEulerAngles.z); 
	}

	public void setXY(float mx, float my){
		x = mx;
		y = my;
	}
}
