﻿using UnityEngine;
using System.Collections;

public class TimerFadeController : MonoBehaviour {
	private float fadeTarget = 0.0f;

	private float deltaTime = 0.0f;
	private float fadeTime = 0.05f;
	private float fadeVal = 0.0f;
	private float fadeOutSpeed = 0.1f;
	private float fadeInSpeed = 0.1f;

	private float totalTime = 0.0f;

	private GameObject referee;


	// Use this for initialization
	void Start () {
		fadeVal = renderer.material.color.a;
		referee = GameObject.FindGameObjectWithTag("Referee");
	}
	
	// Update is called once per frame
	void Update () {
		if(fadeTarget < fadeVal + 0.2)
			FadeOut (fadeTarget);
		else
			FadeIn(fadeTarget);

		if(getTime() > 0.0f){
			updateScale();
			fadeTarget = 0.5f;
		}
		else 
			fadeTarget = 0.0f;
	}

	void FadeOut(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal > amount){
				fadeVal -= fadeOutSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}
	
	void FadeIn(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal < amount){
				fadeVal += fadeInSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}

	float getTime(){
		return referee.GetComponent<GameController>().getHoverTime();
	}

	public void setFadeTarget(float am){
		fadeTarget = am;
	}

	public void setFadeVal(float val){
		fadeVal = val;
		Color c = renderer.material.color;
		c.a = fadeVal;
		renderer.material.color = c;
	}

	void getHoverTime(){

	}

	void updateScale(){

	}
}
