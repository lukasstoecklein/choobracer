﻿using UnityEngine;
using System.Collections;

public class TimerExpanderController : MonoBehaviour {
	float hoverTime = 0.0f;
	private GameObject referee;

	private Vector3 defaultScale;

	// Use this for initialization
	void Start () {
		referee = GameObject.FindGameObjectWithTag("Referee");	
		defaultScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		float min = 0.24f;
		float max = 1.0f;
		Vector3 targetScale = new Vector3(0,0,0);

		string curTargetName = referee.GetComponent<GameController>().getCurTargetName();

		if(curTargetName != "c") min = 0.0f;

		float s = Mathf.Lerp(min,max,(hoverTime-getTimeHovered())/(hoverTime));


		if(s < Mathf.Infinity && s > 0)
			targetScale = new Vector3(s,s,transform.localScale.z);

		if(s == 1)
			targetScale = new Vector3(0,0,transform.localScale.z);		

		transform.localScale = Vector3.Lerp (transform.localScale, targetScale, Time.deltaTime*100);
	}

	public void setHoverTime(float f){
		hoverTime = f;
	}

	float getTimeHovered(){
		return referee.GetComponent<GameController>().getHoverTime();
	}
}
