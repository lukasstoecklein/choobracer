﻿using UnityEngine;
using System.Collections;

public enum TimerType{
	HoverTime, LapTime, LastLapTimeFinal	
}


public class DisplayTime : MonoBehaviour {
	public GameObject referee;
	float curTime = 0.0f;	
	public TimerType type;

	// Use this for initialization
	void Start () {
		GetComponent<TextMesh>().text = "" + curTime;
	}

	void Update(){
		if(type == TimerType.HoverTime)
			curTime = referee.GetComponent<GameController>().getHoverTime();
		if(type == TimerType.LapTime)
			curTime = referee.GetComponent<GameController>().getLapTime();
		if(type == TimerType.LastLapTimeFinal)
			curTime = referee.GetComponent<GameController>().getLastLapTimeFinal();
		GetComponent<TextMesh>().text = curTime.ToString("F1");
	}

	void setTimer(float time){
		curTime = time;
	}
}
