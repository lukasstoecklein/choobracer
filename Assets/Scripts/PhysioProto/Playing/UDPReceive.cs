﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class UDPReceive : MonoBehaviour
{
	public int port = 5555;
	private UdpClient client;
	private IPEndPoint RemoteIpEndPoint;
	private Thread t_udp;
	
	private string lastValidUDP = ":";
	
	void Start()
	{
		client = new UdpClient(port);
		RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 5555);
		t_udp = new Thread(new ThreadStart(UDPRead));
		t_udp.Name = "Mindtuner UDP thread";
		t_udp.Start();
	}
	
	public void UDPRead()
	{
		print("listening UDP port " + port);
		while (true)
		{
			try
			{
				byte[] receiveBytes = client.Receive(ref RemoteIpEndPoint);
				string returnData = Encoding.ASCII.GetString(receiveBytes);
				// parsing
				print(returnData);
				lastValidUDP = returnData;
			}
			catch (Exception e)
			{
				print("Not so good " + e.ToString());
			}
			Thread.Sleep(20);
		}
	}
	
	public string getLastUDP()
	{
		return lastValidUDP;
	}
	
	void Update()
	{
		//if (t_udp != null) 
		//print(t_udp.IsAlive);
		print (getLastUDP());
	}
	
	void OnDisable()
	{
		if (t_udp != null) t_udp.Abort();
		client.Close();
	}
}