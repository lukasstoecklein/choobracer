﻿using UnityEngine;
using System.Collections;

public class GameControllerBUP : MonoBehaviour {
	private string gameName1 = "Central Balance (10sec)";
	private string gameRun1 = "c 5";

	private string gameName2 = "Lap (Medium Difficulty)";	
	private string gameRun2 = "c 2 oN oNW oW oSW oS oSE oE oNE oN c iS iSW iW iNW iN iNE iE iSE iS oN 1 oSW 1 oE 1 oNW 1 oS 1";

	private string gameName3 = "Lap (Hard difficulty)";
	private string gameRun3 = "c 0.5 iN iNE iE iSE iS iSW iW iNW iN c 1 oNE iE oSE iS oSW iW oNW iN c 1 iN 0.5 oN 0.5 iN 0.5 c 1 iS 0.5 oS 0.5 is 0.5 c 1 iW 0.5 oW 0.5 iW 0.5 c 1 iE 0.5 oE 0.5 iE 0.5 c 1 iN 0.5 oN 0.5 iN 0.5 c 1 iE 0.5 oE 0.5 iE 0.5 c 1 iS 0.5 oS 0.5 iS 0.5 c 1 iW 0.5 oW 0.5 iW 0.5 c 1";

	private string gameName4 = "Lap (Inner rim clockwise)";
	private string gameRun4 = "c 1 iN iNE iE iSE iS iSW iW iNW iN c 1";	

	private string gameName5 = "Lap (Inner rim counterclockwise)";
	private string gameRun5 = "c 1 iN iNW iW iSW iS iSE iE iNE iN c 1";

	private string testString = "";

	public LineRenderer lineRenderer;
	private Color c1 = Color.yellow;
	private Color c2 = Color.red;
	private Vector2[] points;

	private string[] instructions;
	private int curInstructNum = 0;
	private string gameName = "Central Balance (10sec)";

	private bool hasHoverTime = false;
	private float hoverTime = 0.0f;
	
	private GameObject[] targets;
	private GameObject curTarget;

	private GameObject pointer;

	private GameObject[] timers;
	private GameObject[] trails;
	
	public GameObject timerFade;
	public GameObject timerExpander;

	private float startTime = 0.0f; 
	private float lastLapTime = 0.0f;
	private float timeSinceLastTrailSpawn = 0.0f;
	private Vector3 lastSpawnPosition;
	private GameObject lastSpawnedSpawn;
	private float trailSpawnDistTime = 0.1f;
	private bool targetHit = false;

	private bool playing = false;
	private bool hasShownTrail = false;
	private bool isRunningReplay = false;

	public float sliderValue = 0.0f;
	private float lastSliderValue = 0.0f;
	private float replayCounter = 0;
	private float lastReplayIncrement = 0.0f;
	public GUIContent replayBtnContent;
	public GUIStyle replayBtnStyle;

	public  GUIStyle slider, thumb;	//for the horizontal scroller
	public GameObject replayTarget;

	private int trailNum = 0;

	
	bool toggleStrictHover = false;


	// Use this for initialization
	void Start () {
		testString = gameRun1;
		targets = GameObject.FindGameObjectsWithTag("Target");
		timers = GameObject.FindGameObjectsWithTag("Timer");
		pointer = GameObject.FindGameObjectWithTag("Player");
		instructions = testString.Split(' ');
		//getTarget ();
		startTime = Time.time;

		lineRenderer.GetComponent<LineRenderer>().SetWidth(0.03f,0.03f);
	}
	
	// Update is called once per frame
	void Update () {
		if(!playing)
			checkForSettingsInput();

		if(playing){
			if(curInstructNum > instructions.Length){
				lastLapTime = getLapTime();
				playing = false;
			}

			if(curTarget)
				if((!toggleStrictHover || curTarget.GetComponent<VisualController>().getIsTriggering()) && (!hasHoverTime || hoverTime <= 0.0f)){
					curTarget.GetComponent<VisualController>().playPulse();
					nextInstruction ();
				}
				else
					curTarget.GetComponent<VisualController>().setCurrent();

			//if hovertime and target is triggering, decrement hovertime left
			if(hasHoverTime && !toggleStrictHover || hasHoverTime && curTarget.GetComponent<VisualController>().getIsTriggering())
				hoverTime -= Time.deltaTime;

			spawnTrail();
			setReplayTargetPos();
		}

		//disable all target highlights
		if(!playing){
			foreach(GameObject t in targets){
				t.GetComponent<VisualController>().deCurrentify();
			}
			replayTarget.SetActive (true);
		}

		//run ended, show the final trail and put into sorted array for replay
		if(!playing && !hasShownTrail){
			showTrail();
		}

		if(!playing && lastSliderValue != sliderValue){ //this triggers if the slider has been moved - so update which trail is showing.
			showTrailAtPoint(Mathf.CeilToInt(sliderValue));
			lastSliderValue = sliderValue;
			replayTarget.GetComponent<ReplayTargetControl>().updateTarget((int)lastSliderValue);
		}

		//replay
		if(!playing && isRunningReplay){	
			//replayTarget.SetActive (true);
			if(replayCounter > trails.Length){
				isRunningReplay = false;
			}
			else{
				if(Time.time - lastReplayIncrement > trailSpawnDistTime){
					sliderValue = replayCounter;
					replayCounter++;
				}
			}
		}
		if (!playing && !isRunningReplay) {
			//replayTarget.SetActive (false);
		}
	}

	public void nextInstruction(){
		targetHit = true;
		curInstructNum++;
		if(curTarget)
			replayTarget.GetComponent<ReplayTargetControl> ().setNewPos (trailNum, curTarget.transform.position);
		getTarget();
	}

	void getTarget(){
		if(curInstructNum < instructions.Length){	
			string curInstruct = instructions[curInstructNum];

			if(curTarget)
				curTarget.GetComponent<VisualController>().deCurrentify();

			//find target in the list of targets
			foreach(GameObject t in targets){
				if(t.name == curInstruct){
					curTarget = t;
					t.GetComponent<VisualController>().setCurrent();

					if(curInstructNum < instructions.Length -1){	//get hover time if it exists
						hasHoverTime = float.TryParse(instructions[curInstructNum+1], out hoverTime);	//puts time into timeToHover, if time is in the next 
						if(hasHoverTime){
							curInstructNum++;
							timerFade.GetComponent<TimerFadeController>().setFadeTarget(hoverTime);
							timerExpander.GetComponent<TimerExpanderController>().setHoverTime(hoverTime);
						}
					}
				}
			}
		}
	}

	public void spawnTrail(){
		if(Time.time - timeSinceLastTrailSpawn > trailSpawnDistTime || targetHit){
			GameObject instance = null;
			targetHit = false;
			if(targetHit){
				//trail spawn indicating target hit
				instance = (GameObject)Instantiate(Resources.Load("targetHit", typeof(GameObject)));
				targetHit = false;	
			}
			else
				instance = (GameObject)Instantiate(Resources.Load("darkTarget", typeof(GameObject)));

			instance.transform.position = pointer.transform.position;
			instance.renderer.enabled = false;
			instance.GetComponent<trailNumberID>().setNum(trailNum);
			trailNum++;

			//lineRenderer.GetComponent<LineRenderer>().SetColors(c1, c2);
			lineRenderer.GetComponent<LineRenderer>().SetVertexCount(trailNum);
			lineRenderer.GetComponent<LineRenderer>().SetPosition(trailNum-1, pointer.transform.position);
		}
	}

	private void setReplayTargetPos(){

	}
	
	void showTrail(){
		trails = GameObject.FindGameObjectsWithTag("Trail");
		points = new Vector2[trails.Length];
		//foreach (GameObject t in trails) {
		for(int i = 0; i < trails.Length; i++){
			GameObject t = trails[i];
			t.renderer.enabled = true;
			/*Color oldColor2 = t.renderer.material.color;
			Color newColor2 = new Color(oldColor2.r, oldColor2.b, oldColor2.g, 1.0f);          
			t.renderer.material.SetColor("_Color", newColor2); 
			//trails[n].transform.localScale = bigScale;
			*/
			points[i] = t.transform.position;
		}

		hasShownTrail = true;
	}

	public void showTrailAtPoint(int n){
		float fadeAmount = 0.0f;
		Vector3 bigScale = new Vector3 (0.5f, 0.5f, 0.5f);
		Vector3 smallScale = new Vector3 (0.2f, 0.2f, 0.2f);

		if(n == trails.Length)
			showTrail();	//show all if at end of slider
		else{
			foreach(GameObject t in trails){
				//t.renderer.enabled = false;
				Color oldColor = t.renderer.material.color;
				Color newColor = new Color(oldColor.r, oldColor.b, oldColor.g, fadeAmount);          
				t.renderer.material.SetColor("_Color", newColor); 
				t.transform.localScale = smallScale;
			}
			//trails[n].renderer.enabled = true;
			Color oldColor2 = trails[n].renderer.material.color;
			Color newColor2 = new Color(oldColor2.r, oldColor2.b, oldColor2.g, 1.0f);          
			trails[n].renderer.material.SetColor("_Color", newColor2); 
			trails[n].transform.localScale = bigScale;
		}
	}

	public void showTrailAtPointa(int n){

	}

	public float getHoverTime(){
		return hoverTime;
	}

	public string getCurTargetName(){
		if(curTarget)
			return curTarget.name;
		return "No target";
	}

	public float getLapTime(){
		if(playing)
			return Time.time - startTime;
		return getLastLapTimeFinal();
	}

	public float getLastLapTimeFinal(){
		return lastLapTime;
	}

	void OnGUI(){
		if(!playing){
			//toggleStrictHover = GUI.Toggle(new Rect(10, Screen.height - 30, 130, 30), toggleStrictHover, "use this if you need it ok kiddo");
			GUI.Label(new Rect(10, Screen.height - 100, 300, 100), "Numbers 1-5 change game type.");
			GUI.Label(new Rect(10, Screen.height - 70, 300, 100), "Current game type: \n" + gameName + ".");
			GUI.Label(new Rect(10, Screen.height - 30, 300, 100), "Press space to start!");
		}

		if(trails != null){
			if(!playing && trails.Length > 0){
				sliderValue = GUI.VerticalSlider(new Rect(Screen.width - 55, Screen.height/2 - 200, 40, 400), (int)sliderValue, 0.0F, trails.Length, slider, thumb);
				if (GUI.Button(new Rect(Screen.width - 55, Screen.height/2 + 210, 50, 50), replayBtnContent, replayBtnStyle)){
					if(!isRunningReplay){
						isRunningReplay = true;
						lastReplayIncrement = Time.time;
						if(sliderValue == trails.Length)
							sliderValue = 0;
						replayCounter = sliderValue;
					}
					else{	//pause on click if running
						isRunningReplay = false;
					}
				}
			}
		}
	}

	void restartLap(){
		playing = true;
		hasShownTrail = false;
		curInstructNum = -1;
		nextInstruction ();
		startTime = Time.time;
		trailNum = 0;
		replayTarget.SetActive (false);
		replayTarget.GetComponent<ReplayTargetControl> ().resetAll();

		//destroy last trail
		foreach(GameObject t in trails)
			Destroy(t);
	}

	void checkForSettingsInput(){
		if(Input.GetKeyUp("space")) //starting a new lap when the last one finishes
			restartLap();

		if(Input.GetKeyDown(KeyCode.Alpha1)){ //starting a new lap when the last one finishes
			testString = gameRun1;
			instructions = testString.Split(' ');
			gameName = gameName1;
			toggleStrictHover = false;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha2)){ //starting a new lap when the last one finishes
			testString = gameRun2;
			instructions = testString.Split(' ');
			gameName = gameName2;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha3)){ //starting a new lap when the last one finishes
			testString = gameRun3;
			instructions = testString.Split(' ');
			gameName = gameName3;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha4)){ //starting a new lap when the last one finishes
			testString = gameRun4;
			instructions = testString.Split(' ');
			gameName = gameName4;
			toggleStrictHover = true;
		}
		else if(Input.GetKeyDown(KeyCode.Alpha5)){ //starting a new lap when the last one finishes
			testString = gameRun5;
			instructions = testString.Split(' ');
			gameName = gameName5;
			toggleStrictHover = true;
		}
	}
}

	//only deprecate timer on targets that you have to hover over when the icon is close enough, if theyve been over it for 0.5 secs

