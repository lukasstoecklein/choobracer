﻿using UnityEngine;
using System.Collections;

public class ReplayTargetControl : MonoBehaviour {
	int numsSize = 50;
	int possSize = 50;
	int[] nums = new int[50];
	Vector2[] positions = new Vector2[50];
	Vector2 curPos = new Vector2(0,0);

	private int count = 0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector2.Lerp (transform.position, curPos, Time.deltaTime*10);
	}

	//num = the trail count that the trigger starts from
	public void setNewPos(int num, Vector2 pos){
		if(count > numsSize-1)
			increaseArraySizes();

		nums [count] = num;
		positions [count] = pos;
		count++;
	}

	private void increaseArraySizes(){
		numsSize += 10;
		possSize += 10;
		//make both arrays bigger
		int[] tempNums = new int[numsSize];
		Vector2[] tempPoss = new Vector2[possSize];
		for(int i = 0; i < nums.Length; i++){
			tempNums[i] = nums[i];
			tempPoss[i] = positions[i];
		}
		nums = tempNums;
		positions = tempPoss;
	}

	//n = current trail point on the replay
	public void updateTarget(int n){
		if(n<nums[count-1]){
			for(int i = 0; i < nums.Length; i++){
				if(n < nums[i]){
					curPos = positions[i];
					break;
				}
			}
		}
	}

	public void resetAll(){
		numsSize = 50;
		possSize = 50;
		nums = new int[50];
		positions = new Vector2[50];
		count = 0;
	}
}
