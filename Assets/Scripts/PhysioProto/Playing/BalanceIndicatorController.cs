﻿using UnityEngine;
using System.Collections;

public class BalanceIndicatorController : MonoBehaviour {
	private float maxSpeed = 50.0f;
	private float moveForce = 15.0f;
	private float circleRadius = 4.7f;
	private Vector2 center = new Vector2(0,0);

	// Use this for initialization
	void Start () {}

	void FixedUpdate(){
		constrainToCircle();
		followCursor();
	}
	
	// Update is called once per frame
	void Update () {}

	private void followCursor(){
		if(isMouseInRange()){
			rigidbody2D.velocity = (Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position))/4;
		}
	}

	private void constrainToCircle(){
		Vector2 diff = new Vector2(transform.position.x, transform.position.y) - center;
		float distance = diff.magnitude;

		if (distance > circleRadius) { 
			transform.position = center + (diff / distance) * circleRadius;
			rigidbody2D.AddForce(-1 * rigidbody2D.velocity);
		}
	}

	private bool isMouseInRange(){
		Vector2 diff = Input.mousePosition - Camera.main.WorldToScreenPoint(new Vector3(center.x,center.y,0));
		return diff.magnitude < 1000;
	}
}
