﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Screen.showCursor = true;
		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		string ex = "oN 0 oNE 0 oE 0 oSE 0 oS 0 oSW 0 oW 0 oNW 0 iN 0 iNE 0 iE 0 iSE 0 iS 0 iSW 0 iW 0 iNW 0 c 0 iS 0 iSW 0 iW 0 iNW 0 iN 0 iNE 0 iE 0 iSE 0 oS 0 oSW 0 oW 0 oNW 0 oN 0 oNE 0 oE 0 oSE 0";
		string exName = "Spirals";
		bool foundEx = false;

		for (int i = 0; i < GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().getExerciseNames().Length; i++) {
			if(GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().getExerciseNames()[i] == exName) foundEx = true;
		}
		if (!foundEx) {
			//add name to names list
			GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().saveExerciseName(exName);
			//add playerpref 
			PlayerPrefs.SetString(exName, ex);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.A))
		   Application.LoadLevel("PlayAccuracy");
	}

	void OnGUI(){
		//if (GUI.Button(new Rect(10, 10, Screen.width/2-20, Screen.height-20), "", playBtnStyle))
			//Application.LoadLevel(2);
	}
}
