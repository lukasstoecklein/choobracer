﻿using UnityEngine;
using System.Collections;

public class ViewButtonControl : MonoBehaviour {
	public Sprite normTex;
	public Sprite hoverTex;
	public Sprite pressTex;

	private bool mouseDown = false;
	private bool mouseOver = false;

	private bool shrink = true;

	private float hoverScale = 1.2f;
	private float normScale = 1.0f;
	private float scaleSpeed = 12.0f;
	private float shrinkSpeed = 12.0f;
	private float scaleX = 1.0f;
	private float scaleY = 1.0f;

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {
		if(shrink){
			scaleX = Mathf.Lerp(transform.localScale.x, 0, Time.deltaTime*shrinkSpeed);
			scaleY = Mathf.Lerp(transform.localScale.y, 0, Time.deltaTime*shrinkSpeed);
			transform.localScale = new Vector2(scaleX, scaleY);
			GetComponent<SpriteRenderer>().sortingLayerName = "Pointers";
		}
		else{
			if(mouseOver){
				scaleX = Mathf.Lerp(transform.localScale.x, hoverScale, Time.deltaTime*scaleSpeed);
				scaleY = Mathf.Lerp(transform.localScale.y, hoverScale, Time.deltaTime*scaleSpeed);
				transform.localScale = new Vector2(scaleX, scaleY);
			}
			else{
				scaleX = Mathf.Lerp(transform.localScale.x, normScale, Time.deltaTime*scaleSpeed);
				scaleY = Mathf.Lerp(transform.localScale.y, normScale, Time.deltaTime*scaleSpeed);
				transform.localScale = new Vector2(scaleX, scaleY);
			}
			GetComponent<SpriteRenderer>().sortingLayerName = "Default";
		}
	}

	void OnMouseExit(){
		mouseOver = false;
		GetComponent<SpriteRenderer>().sprite = normTex;
	}
	
	void OnMouseOver(){
		mouseOver = true;
		if(!mouseDown)
			GetComponent<SpriteRenderer>().sprite = hoverTex;
	}

	void OnMouseDown(){
		mouseDown = true;
		GetComponent<SpriteRenderer>().sprite = pressTex;
	}

	void OnMouseUp(){
		mouseDown = false;
	}

	public void setShrink(bool b){
		shrink = b;
	}
}
