﻿using UnityEngine;
using System.Collections;

public class FinalPlayButtonControl : MonoBehaviour {
	public Sprite normTex;
	public Sprite hoverTex;
	public Sprite pressTex;

	private bool mouseDown = false;
	private bool mouseOver = false;

	private bool shrink = true;
	private bool hasLoadedData = false;

	private float hoverScale = 1.2f;
	private float normScale = 1.0f;
	private float scaleSpeed = 12.0f;
	private float shrinkSpeed = 12.0f;
	private float scaleX = 1.0f;
	private float scaleY = 1.0f;

	public GUIStyle listStyle;
	public GUIStyle btnStyle;
	public GUIStyle clickToChangeStyle;
	private bool showList = false;
	private int listEntry = 0;
	private GUIContent[] exNamesContent;
	private ComboBox comboBoxControl;


	// Use this for initialization
	void Start () {
		/*listStyle.normal.textColor = Color.black; 
		listStyle.onHover.background =
		listStyle.hover.background = new Texture2D(2, 2);
		listStyle.padding.left =
		listStyle.padding.right =
		listStyle.padding.top =
		listStyle.padding.bottom = 4;*/
		
		/*btnStyle.normal.textColor = Color.black; 
		btnStyle.padding.left =
		btnStyle.padding.right =
		btnStyle.padding.top =
		btnStyle.padding.bottom = 4;*/
	}
	
	// Update is called once per frame
	void Update () {
		if(shrink){
			scaleX = Mathf.Lerp(transform.localScale.x, 0, Time.deltaTime*shrinkSpeed);
			scaleY = Mathf.Lerp(transform.localScale.y, 0, Time.deltaTime*shrinkSpeed);
			transform.localScale = new Vector2(scaleX, scaleY);
			GetComponent<SpriteRenderer>().sortingLayerName = "Pointers";
		}
		else{
			if(!hasLoadedData){
				int numNames = GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().getNumNames();
				exNamesContent = new GUIContent[numNames];
				string[] exNames = PlayerPrefsX.GetStringArray("ExerciseNames");
				for(int i = 0; i < numNames; i++){
					exNamesContent[i] = new GUIContent(exNames[i]);
				}
				float width = Screen.width/2.5f;
				comboBoxControl = new ComboBox(new Rect(Screen.width/2-width/2, Screen.height/2.5f, width, 30), new GUIContent(exNamesContent[0]), exNamesContent, "button", "box", btnStyle, listStyle);
				hasLoadedData = true;
			}
			if(mouseOver){
				scaleX = Mathf.Lerp(transform.localScale.x, hoverScale, Time.deltaTime*scaleSpeed);
				scaleY = Mathf.Lerp(transform.localScale.y, hoverScale, Time.deltaTime*scaleSpeed);
				transform.localScale = new Vector2(scaleX, scaleY);
			}
			else{
				scaleX = Mathf.Lerp(transform.localScale.x, normScale, Time.deltaTime*scaleSpeed);
				scaleY = Mathf.Lerp(transform.localScale.y, normScale, Time.deltaTime*scaleSpeed);
				transform.localScale = new Vector2(scaleX, scaleY);
			}
			GetComponent<SpriteRenderer>().sortingLayerName = "Default";
		}
	}

	void OnMouseExit(){
		mouseOver = false;
		GetComponent<SpriteRenderer>().sprite = normTex;
	}
	
	void OnMouseOver(){
		mouseOver = true;
		if(!mouseDown)
			GetComponent<SpriteRenderer>().sprite = hoverTex;
	}

	void OnMouseDown(){
		mouseDown = true;
		GetComponent<SpriteRenderer>().sprite = pressTex;
	}

	void OnMouseUp(){
		mouseDown = false;
		if(mouseOver){	//this is here because this can trigger if mouseDown was on the object but mouseUp wasn't.
			string ex = (PlayerPrefs.GetString(exNamesContent[comboBoxControl.SelectedItemIndex].text));
			GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().setCurEx(ex);
			Application.LoadLevel("PlayTargets");
		}
	}
	
	public void setShrink(bool b){
		shrink = b;
	}

	void OnGUI(){
		if(!shrink){
			comboBoxControl.Show();
			float width = Screen.width/2.5f;
			GUI.Label(new Rect(Screen.width/2, Screen.height/2.7f, 20, 10), "Click to change", clickToChangeStyle);
		}
	}
}
