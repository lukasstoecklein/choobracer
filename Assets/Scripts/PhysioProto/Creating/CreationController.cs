﻿using UnityEngine;
using System.Collections;

public class CreationController : MonoBehaviour {
	private string exName = "Enter name here";
	private Vector2 scrollPosition;
	private string exerciseTargetsStr = "";
	public LineRenderer lineRenderer;
	private bool texterased = false;
	private bool bShowLine = false;
	public GUIStyle SaveBtnStyle;
	public GUIStyle CancelBtnStyle;
	public Texture NameTex;
	public Texture TgtOrderTex;
	public Texture TgtTimerTex;
	private int numTargetTimer = 0;

	// Use this for initialization
	void Start () {
		lineRenderer.GetComponent<LineRenderer>().SetWidth(0.2f,0.2f);	
	}
	
	// Update is called once per frame
	void Update () {}

	void OnGUI(){
		//name label
		GUI.Label(new Rect(Screen.width/2+95, 23, 100, 30), NameTex);

		//name text area
		GUI.SetNextControlName ("exNameTxtAr");
		exName = GUI.TextArea(new Rect(Screen.width/2+103, 50, Screen.width/2-200, 23), exName, 200);
		if (GUI.GetNameOfFocusedControl () == "exNameTxtAr"){
			if (texterased == false){
				exName = "";
				texterased = true;
			}
		}

		//target order label
		GUI.Label(new Rect(Screen.width/2+95, 93, 210, 30), TgtOrderTex);
		
		//timings add button
		GUI.Label (new Rect(Screen.width - 210, 93, 100, 30),TgtTimerTex);
		
		//timings box
		string text = GUI.TextField(new Rect(Screen.width - 135, 100, 30, 20), numTargetTimer.ToString());
		int temp = 0;
		if (int.TryParse(text, out temp))
		{
			numTargetTimer = temp;
		}
		else if (text == "") numTargetTimer = 0;

		//target string box
		GUILayout.BeginArea (new Rect(Screen.width/2+100, 120, Screen.width/2-50, Screen.height - 123 - Screen.height/3));  
		scrollPosition = GUILayout.BeginScrollView (scrollPosition, GUILayout.Width (Screen.width/2-200), GUILayout.Height (Screen.height - 123 -Screen.height/3));
		GUI.skin.box.wordWrap = true; 
		GUILayout.Box(exerciseTargetsStr);      
		GUILayout.EndScrollView ();
		GUILayout.EndArea();

		//discard button
		if (GUI.Button (new Rect(Screen.width - 100, 123, 70, 25), "Discard"))
			exerciseTargetsStr = "";
						
	}

	public void addTarget(string s){
		exerciseTargetsStr += s + " " + numTargetTimer + " ";
	}

	public string getName(){
		print ("name: " + exName);
		return exName;
	}

	public string getEx(){
		print ("ex: " + exerciseTargetsStr);
		return exerciseTargetsStr;
	}
}


