using UnityEngine;
using System.Collections;
using System.IO.Ports;

public class CreationMovementController : MonoBehaviour {
	private float speed = 20.0f;

	//all these get calculated later
	private float moveX = 0.0f;
	private float moveY = 0.0f;

	private Vector3 center3 = new Vector3(0,0,0);

	// Use this for initialization
	void Start () {}

	void FixedUpdate(){
		mouseMove();
		constrainToCircle();
	}
	
	// Update is called once per frame
	void Update () {
		moveX = Input.GetAxis("Horizontal");
		moveY = Input.GetAxis("Vertical");
	}

	private void mouseMove(){
		transform.position = Vector2.Lerp (transform.position, new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y), Time.deltaTime*10);
	}

	private void constrainToCircle(){
		float boardMax = 4.7f;
		if(Vector2.Distance(transform.position, center3) > boardMax){
			Vector2 newPos = transform.position - center3;
			newPos.Normalize();
			newPos *= boardMax;
			transform.position = newPos;
			Screen.showCursor = true;
		}
		else
			Screen.showCursor = true;
	}
}
