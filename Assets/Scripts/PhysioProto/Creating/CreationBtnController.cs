﻿using UnityEngine;
using System.Collections;

public class CreationBtnController : MonoBehaviour {
	private float scaleX = 0.0f;
	private float scaleY = 0.0f;
	private float hoverScale = 1.2f;
	private float normScale = 1.0f;
	private float scaleSpeed = 12.0f;
	private bool mouseOver = false;

	public GameObject Referee;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(mouseOver){
			scaleX = Mathf.Lerp(transform.localScale.x, hoverScale, Time.deltaTime*scaleSpeed);
			scaleY = Mathf.Lerp(transform.localScale.y, hoverScale, Time.deltaTime*scaleSpeed);
			transform.localScale = new Vector2(scaleX, scaleY);
		}
		else{
			scaleX = Mathf.Lerp(transform.localScale.x, normScale, Time.deltaTime*scaleSpeed);
			scaleY = Mathf.Lerp(transform.localScale.y, normScale, Time.deltaTime*scaleSpeed);
			transform.localScale = new Vector2(scaleX, scaleY);
		}
	}

	void OnMouseExit(){
		mouseOver = false;
		//GetComponent<SpriteRenderer>().sprite = normTex;
	}
	
	void OnMouseOver(){
		mouseOver = true;
		//if(!mouseDown)
			//GetComponent<SpriteRenderer>().sprite = hoverTex;
	}

	void OnMouseUp(){
		if (mouseOver) {	//this is here because this can trigger if mouseDown was on the object but mouseUp wasn't.
			if(name == "SaveExerciseBtn"){
				//do the saving here
				string exname = Referee.GetComponent<CreationController>().getName();
				string ex = Referee.GetComponent<CreationController>().getEx();
				print (exname);
				print (ex);
				//add name to names list
				GameObject.Find("PersistentDataObj").GetComponent<PersistentData>().saveExerciseName(exname);
				//add playerpref 
				PlayerPrefs.SetString(exname, ex);
				Application.LoadLevel("MainMenu");
			}
			else if(name == "CancelExerciseBtn"){
				Application.LoadLevel("MainMenu");
			}
		}
	}
}
