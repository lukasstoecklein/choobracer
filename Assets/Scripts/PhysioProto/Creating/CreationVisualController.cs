﻿using UnityEngine;
using System.Collections;

public class CreationVisualController : MonoBehaviour {
	private float deltaTime = 0.0f;
	private float fadeTime = 0.05f;
	private float fadeVal = 1.0f;
	private float minFade = 0.5f;

	private float fadeInSpeed = 0.3f;
	private float fadeOutSpeed = 0.1f;

	private bool isFadingIn = false;
	private bool isFadingOut = false;
	private bool pulseFadeIn = true;
	private bool pulseFadeOut = false;
	private bool isHovering = false;
	private bool isCurrent = false;
	private bool isTriggering = false;

	private GameObject referee;

	// Use this for initialization
	void Start () {
		Color c = renderer.material.color;
		c.a = 0.0f;
		renderer.material.color = c;
		referee = GameObject.Find ("Referee");
	}
	
	// Update is called once per frame
	void Update () {		
		if(isHovering)
			FadeIn(1.0f);
		else 
			FadeOut(minFade);
	}

	void FadeIn(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal < amount){
				fadeVal += fadeInSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}
	
	void FadeOut(float amount){
		if(Time.time - deltaTime > fadeTime){
			deltaTime = Time.time;
			if(fadeVal > amount){
				fadeVal -= fadeOutSpeed;
				Color c = renderer.material.color;
				c.a = fadeVal;
				renderer.material.color = c;
			}
		}
	}

	void OnMouseEnter(){
		isHovering = true;
		if(Input.GetMouseButton(0))
			referee.GetComponent<CreationController> ().addTarget (name);
	}

	void OnMouseExit(){
		isHovering = false;
	}

	void OnMouseDown(){
		referee.GetComponent<CreationController> ().addTarget (name);
	}
}
