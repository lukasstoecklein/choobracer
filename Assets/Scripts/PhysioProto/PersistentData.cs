﻿using UnityEngine;
using System.Collections;

public class PersistentData : MonoBehaviour {
	private string[] exerciseNames;
	private int numNames = 0;
	private string curExercise = "iSW 5";

	void Awake() {
		DontDestroyOnLoad(this);
	}

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll();
		exerciseNames = PlayerPrefsX.GetStringArray("ExerciseNames");
		if(getNumNames() == 0)
			exerciseNames = new string[50];
		else
			numNames = getNumNames();
	}

	//saves the name of the exercise so we can use it as the key to the exercise it describes
	public void saveExerciseName(string name){
		exerciseNames[numNames] = name;

		if (!PlayerPrefsX.SetStringArray("ExerciseNames", exerciseNames))
			print("Can't save names");

		numNames++;
	}
	
	public void deleteExercise(string name){
		PlayerPrefs.DeleteKey(name);

		int namePos = 0;
		for(int i = 0; i < getNumNames(); i++){
			string n = exerciseNames[i];
			if(n == name){
				namePos = i;
				break;
			}
		}

		//shift everything down, overwriting the deleted name.
		for(int j = namePos; j < getNumNames() - 2; j++){
			exerciseNames[j] = exerciseNames[j+1];
		}
		exerciseNames[exerciseNames.Length-1] = "";
	}

	public string[] getExerciseNames(){
		return exerciseNames;
	}

	public string getCurEx(){
		return curExercise;
	}

	public void setCurEx(string s){
		curExercise = s;
	}

	public int getNumNames(){
		int n = 0;
		for(int i = 0; i < exerciseNames.Length; i++){
			if(exerciseNames[i] == "")
				return n;
			n++;
		}
		return n;
	}
}
