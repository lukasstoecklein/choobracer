﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameProfileHolder : MonoBehaviour {
	private List<GameProfile> gamesList;
	static GameProfile currentGameProfile;
	public static GameProfileHolder instance;

	// game images
	public Texture choobRacerBackground;
	public Texture snowStormBackground;
	public Texture physioProtoBackground;
	// Use this for initialization
	void Start () {
		// create games list
		gamesList = new List<GameProfile> ();
        gamesList.Add(new GameProfile("Choob Racer", "Pilot your spaceship through a hypertube while navigating obstacles and picking up powerups!", choobRacerBackground, "Choobmenu"));
		gamesList.Add (new GameProfile("Powderdrift","A snowboarding game featuring slalom courses, massive jumps and grinding bars!", snowStormBackground, "PlayPowderDrift"));
		gamesList.Add (new GameProfile("Zen Master","An application that lets you test your performance quickly, you can also create your own tests", physioProtoBackground, "MainMenu"));

//		gamesList.Add (new GameProfile("BlastZone","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Blackwings Lair","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("SkyBomber","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("TagTeam","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("RadioAce","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Port Jim","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Jump Jump","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Court Story","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Code Implex","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Dead Leaf","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("DistelHouse","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Mirrors Corner","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Zen Master","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Turret Hero","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Waiter","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Dead Leaf","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("DistelHouse","", snowStormBackground, "PlayPowderDrift"));
//		gamesList.Add (new GameProfile("Mirrors Corner","", snowStormBackground, "PlayPowderDrift"));




		currentGameProfile = gamesList [0];
		print ("ONLY CALLED ONCE ::::::::::::::::::::::::::::::::::::::::::::::::::");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void Awake(){
		transform.parent = null;
		instance = this;
		DontDestroyOnLoad (gameObject);
	}
	
	void setGameScores(List<GameScore> scores){
		
	}
	public GameProfile getGameProfile(){
		return currentGameProfile;
	}

	public void setGameProfile(GameProfile gameProfile){
		currentGameProfile = gameProfile;
	}
	public List<GameProfile> getGamesList(){
		return gamesList;
	}

	public void setGamesList(List<GameProfile> gamesList){
		this.gamesList = gamesList;
	}
}
