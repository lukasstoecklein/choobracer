﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net;
using System.Net.Sockets;

public class MainGUIScript : MonoBehaviour {
	//public GameObject ScoreMaster;
	private GameProfileHolder gameProfileHolder;
	List<GameProfile> gamesList;
	public GameProfile selectedGameProfile;
	string[] menusArray = new string[] {"HOME","ARCADE","SOCIAL","PROGRESS"};
	List<Score> scoreboardList;

	private int selectedMenu = 1;
	private int selectedGame = 0;
	private int previousSelectedGame = 0;
	private int selectedGraphOrTable = 0;

	// images - game background images are below
	public Texture mainMenuBG;
	public Texture grayGameBG;
	public Texture navBG;
	public Texture exampleGraph;
	public Texture graphBackground;



	// styles.. styles as far as the eye can see...
	public GUIStyle fileMenuStyle;
	public GUIStyle navMenuStyle;
	public GUIStyle mainBodyStyle;
	public GUIStyle mainBodyStyleLeft;
	public GUIStyle mainBodyStyleRight;
	public GUIStyle gamesTitle;
	public GUIStyle selectedGameTitle;
	public GUIStyle gameButtonStyle;
	public GUIStyle gameAboutStyle;
	public GUIStyle playButtonStyle;
	public GUIStyle graphOrTableStyle;
	public GUIStyle graphStyle;
	public GUIStyle scoreElementStyle;

	// scroll positions
	public Vector2 scrollPosition;
	public Vector2 scoreboardScrollPosition;

	public Graph.Style style; 
	public Graph graph;

	private string IP = "temp IP";


	// Use this for initialization
	void Start () {
		Screen.SetResolution(1020, 610, false);

		gameProfileHolder = GameProfileHolder.instance;
		gamesList = gameProfileHolder.getGamesList ();
		selectedGameProfile = gameProfileHolder.getGamesList() [0];
		//selectedGameProfile = gameProfileHolder.getGameProfile ();
		print ("selected gameProfle is " + selectedGameProfile.getSceneName ());

	//	scoreboardList = gamesList[0].getScores ();

		Screen.fullScreen = false;


		if (selectedGame == 0)
			updateGraph ();

//		graph = new Graph (style);
//		
//		if (selectedGameProfile.getScores ().Count > 0) {
//			double[] y = new double[selectedGameProfile.getScores ().Count];
//			double[] x = new double[y.Length];
//			for (int i = 0; i < selectedGameProfile.getScores().Count ; i++) {
//				x[i] = i;
//				y[i] = selectedGameProfile.getScores()[i].getScore();
//			}
//			
//			graph.Plot (x, y, new Color(0.5f, 0.5f, 0.0f));
//		}

		// get IP address
		IP = LocalIPAddress ();
	}

	void updateGraph(){

		//intialise the graph for use of a particular nature
		style = new Graph.Style ();
		style.BackgroundColor = new Color(0.19f, 0.19f, 0.19f);
		style.PlotBackgroundColor = new Color(0.12f, 0.13f, 0.15f);
		style.AxisTextColor = new Color(0.41f, 0.41f, 0.41f);
		style.AxisTextSize = 10;
		
		style.XPos = 225;
		style.YPos = 295;
		
		style.Width = 760;
		style.Height = 290;
		
		style.PlotMarginX = 60;
		style.PlotMarginY = 25;
		
		style.PlotWidth = 675;
		style.PlotHeight = 200;
		graph = new Graph (style);
		
		if (selectedGameProfile.getScores ().Count > 0) {
			double[] y = new double[selectedGameProfile.getScores ().Count];
			double[] x = new double[y.Length];
			for (int i = 0; i < selectedGameProfile.getScores().Count ; i++) {
				x[i] = i;
				y[i] = selectedGameProfile.getScores()[i].getScore();
			}
			
			graph.Plot (x, y, new Color(0.5f, 0.5f, 0.0f));
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){

		scoreboardList = selectedGameProfile.getScores ();
		


		
		// loading scoreboard




		// draw background texture - blue rivets
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), mainMenuBG);

		// draw nav bar background - grey
		GUI.DrawTexture(new Rect(12, 35, 995, 60), navBG);

		// draw game background texture - gray base
		GUI.DrawTexture (new Rect (15, 96, 990, 491),grayGameBG);

		// transparency fix for game background image - needs to be undone after draw
		GUI.color = new Color(1,1,1,0.6f); //0.5f in c#


		// draw game background texture - game image
		GUI.DrawTexture (new Rect (15, 96, 990, 491),gamesList [selectedGame].getImage (),ScaleMode.ScaleAndCrop);
		
		
		// undoing transparency fix
		GUI.color = Color.white;


		GUILayout.BeginArea (new Rect (0, 0, 1020, 6010));

		GUILayout.BeginVertical();

		//////////////////////////////////////// file menu
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("File", fileMenuStyle))
		{
			// Do something
		}
		if (GUILayout.Button("Options", fileMenuStyle))
		{
			// Do something
		}
		if (GUILayout.Button("View", fileMenuStyle))
		{
			// Do something
		}
		if (GUILayout.Button("Friends", fileMenuStyle))
		{
			// Do something
		}
		if (GUILayout.Button("Help", fileMenuStyle))
		{
			// Do something
		}

		GUILayout.FlexibleSpace ();
		GUILayout.Label ("Your IP address is: "+IP, fileMenuStyle);
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();


		////////////////////////////////////////top nav bar
		selectedMenu = GUILayout.SelectionGrid (selectedMenu, menusArray, 4,navMenuStyle);


		if (selectedMenu == 0) {
			//print ("home");
		}
		if (selectedMenu == 1) {
		//	print ("arcade");
			drawArcade ();
		}
		if(selectedMenu == 2) {
			//	print ("social");

		}
		if (selectedMenu == 3) {
			//	print ("progress");
			
		}

		GUILayout.EndVertical();

		GUILayout.EndArea ();
	}

		// draws the arcade ui inside existing ui
	public void drawArcade(){
		////////////////////////////////// main section - 2 parts - left nav bar and right game info
		GUILayout.BeginHorizontal(mainBodyStyle);
		
		
		/// ////////////////////////////// left nav bar
		GUILayout.BeginVertical(mainBodyStyleLeft);
		
		/// GAMES title
		GUILayout.Label ("GAMES", gamesTitle);
		
		/// Scroll games list
		scrollPosition = GUILayout.BeginScrollView (scrollPosition,false, true, GUILayout.Width (200), GUILayout.Height (470));
		previousSelectedGame = selectedGame;
		selectedGame = GUILayout.SelectionGrid (selectedGame, gamesList.Select (gameProfile => gameProfile.ToString ()).ToArray (), 1, gameButtonStyle);

		if (previousSelectedGame != selectedGame) {
			print ("switched");
			selectedGameProfile = gamesList [selectedGame];
			print (selectedGameProfile.getSceneName());
			updateGraph();
		}


		GUILayout.EndScrollView ();
		
		///////////////////////////////// end left nav bar
		GUILayout.EndVertical();
		

		////////////////////////////////// right game info
		GUILayout.BeginVertical(mainBodyStyleRight);

		// transparency fix  - needs to be undone after draw
		GUI.color = new Color(1,1,1,0.6f); //0.5f in c#

		GUILayout.BeginHorizontal ();
		/// GAMES title
		GUILayout.Label (gamesList[selectedGame].ToString(), selectedGameTitle);
		GUILayout.FlexibleSpace ();

		if (GUILayout.Button ("Play", playButtonStyle)) {
			print (selectedGameProfile.getSceneName ());
			GameProfileHolder.instance.setGameProfile(selectedGameProfile);

			Application.LoadLevel(selectedGameProfile.getSceneName());
		}
		GUILayout.EndHorizontal ();

		/// About heading
		GUILayout.Label (gamesList[selectedGame].getAbout(), gameAboutStyle);



		selectedGraphOrTable = GUILayout.SelectionGrid (selectedGraphOrTable, new string[]{"Progress","Scoreboard","Fitness"}, 3,graphOrTableStyle);

		GUI.color = Color.white;

		// selected graph
		if (selectedGraphOrTable == 0) {
			graph.OnGUI();
			//GUILayout.Box (exampleGraph,graphStyle);
		}

		// selected table
		else if(selectedGraphOrTable == 1){
			// transparency fix  - needs to be undone after draw
			GUI.color = new Color(1,1,1,0.6f); //0.5f in c#
			//print (scoreboardList.Count);
			GUILayout.BeginVertical();
			scoreboardScrollPosition = GUILayout.BeginScrollView (scoreboardScrollPosition,false, true, GUILayout.Width (760), GUILayout.Height (295));
			foreach(Score player in selectedGameProfile.getSortedScores()){
				GUILayout.BeginHorizontal();
				GUILayout.Label(player.getName(),scoreElementStyle);
				GUILayout.FlexibleSpace();
				GUILayout.Label (player.getScore().ToString("F1"), scoreElementStyle);

				GUILayout.EndHorizontal();
			}

			GUILayout.EndScrollView();
			GUILayout.EndVertical();

			GUI.color = Color.white;
		}

		// fitness stuff
		else if(selectedGraphOrTable == 2){
			// transparency fix  - needs to be undone after draw
			GUI.color = new Color(1,1,1,0.6f); //0.5f in c#
			GUILayout.Label ("Targeted Areas", gameAboutStyle);

			GUI.color = Color.white;
		}
	
		

		GUILayout.EndVertical();
		
		/// ///////////////////////////// end main section
		GUILayout.EndHorizontal();

	}
	public string LocalIPAddress()
	{
		IPHostEntry host;
		string localIP = "";
		host = Dns.GetHostEntry(Dns.GetHostName());
		foreach (IPAddress ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				localIP = ip.ToString();
				break;
			}
		}
		return localIP;
	}





}
