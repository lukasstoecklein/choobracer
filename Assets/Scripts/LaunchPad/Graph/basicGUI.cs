﻿//C#
using UnityEngine;
using System.Collections;


public class Foo
{
	public static Graph testGraph;
	public static Graph.Style s;
	static Foo()
	{
		s = new Graph.Style ();
		s.BackgroundColor = new Color(0.19f, 0.19f, 0.19f, 0.0f);
		s.PlotBackgroundColor = new Color(0.12f, 0.13f, 0.15f);
		s.AxisTextColor = new Color(0.41f, 0.41f, 0.41f);
		s.AxisTextSize = 10;

		s.XPos = 50;
		s.YPos = 50;

		s.Width = 760;
		s.Height = 340;

		s.PlotMarginX = 60;
		s.PlotMarginY = 25;

		s.PlotWidth = 675;
		s.PlotHeight = 225;



		testGraph = new Graph (Foo.s);

		double[] y = new double[]{30, 10, 20, 60, 30, 40, 50, 20, 60, 70};
		double[] x = new double[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};


		testGraph.Plot (x, y, new Color(0.5f, 0.5f, 0.0f));
	}
}

public class basicGUI : MonoBehaviour {
	void OnGUI () {


		Foo.testGraph.OnGUI();
	}
}


























