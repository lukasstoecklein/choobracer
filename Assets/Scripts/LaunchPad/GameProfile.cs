﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameProfile : MonoBehaviour {
	
	string name;
	string about;
	Texture image;
	string sceneName;
	double[] scores;
	double[] times;
	List<Score> highScores;
	List<Score> sortedScores;
	bool reversedOrder = false;

	public GameProfile(string name, string about, Texture image, string sceneName){
		this.name = name;
		this.about = about;
		this.image = image;
		this.sceneName = sceneName;
		scores = new double[10];
		times = new double[10];
		highScores = new List<Score>();
	//	highScores.Add (new Score ("Strawman", 1000));

	}

	public string ToString(){ return name; }
	public string getAbout(){ return about; }
	public Texture getImage(){ return image; }
	public string getSceneName(){ return sceneName; }
	public bool newScore(Score score){
		highScores.Add (score);
		print (score.getName ());
		print (highScores.Count);
		highScores.Sort ();
		if (reversedOrder) {
			highScores.Reverse();
		}
		if (getSortedScores()[0] == score) {
			print ("thats a high score!");
			return true;
		}
		return false;
	}
	public List<Score> getScores(){
		return highScores;
	}
	public List<Score> getSortedScores(){
		sortedScores = highScores;
		//sortedScores.Sort ();
		return sortedScores;
	}
	public void setReversal(bool reversal){
		reversedOrder = reversal;
	}

	public Score getHighScore(){
		if (highScores.Count > 0) {
			return highScores [0];
		}
		return null;


	}
	public void Awake(){
		DontDestroyOnLoad (transform.gameObject);
	}
	}
