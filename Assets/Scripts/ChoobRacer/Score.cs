﻿using UnityEngine;
using System.Collections;
using System;

// holds score data for scoreboards - most likely data will be downloaded from server and put into a Score object
public class Score : IComparable<Score>{
	string name;
	double score;
	
	public Score(string name, double score){
		this.name = name;
		this.score = score;
	}
	
	public string getName(){ return name; }
	public double getScore(){ return score; }
	
	public int CompareTo(Score other)
	{
		return score.CompareTo(other.score);
	}
}