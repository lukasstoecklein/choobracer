﻿using UnityEngine;
using System.Collections;

public class SparkController : MonoBehaviour {

	public ParticleSystem sparks;

	// Use this for initialization
	void Start () {
		sparks.enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void turnOn(){
		sparks.enableEmission = true;
	}

	public void turnOff(){
		sparks.enableEmission = false;
	}
}
