﻿using UnityEngine;
using System.Collections;

public class PowerupRotateScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.forward * Time.deltaTime * 100);
		transform.Rotate(Vector3.left * Time.deltaTime * 100);
	}
}
