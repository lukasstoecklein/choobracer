﻿using UnityEngine;
using System.Collections;

public class VisionFlipScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){

		if(other.gameObject.name == "Player1"){
			print ("vision flip on RacerP2");
			GameObject.Find ("Player2").GetComponent<PlayerParentController>().flipScreen ();
		}
		else if(other.gameObject.name == "Player2"){
			print ("vision flip on RacerP1");
			GameObject.Find ("Player1").GetComponent<PlayerParentController>().flipScreen ();
		}
		Destroy(this.gameObject);
	}
}
