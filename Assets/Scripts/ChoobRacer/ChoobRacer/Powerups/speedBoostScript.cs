﻿using UnityEngine;
using System.Collections;

public class speedBoostScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		//print ("speed boost "+ other.gameObject.name);
		if(other.name.Equals("Player1") || other.name.Equals("Player2")){
		//	other.gameObject.GetComponent<PlayerParentController>().speedBoost();
			other.gameObject.GetComponent<PlayerParentController>().boosters = true;
			other.gameObject.GetComponent<PlayerParentController>().boostTime = Time.time;
			other.gameObject.GetComponent<PlayerParentController>().playBoostTransform();
			Destroy(this.gameObject);
		}

	}
}
