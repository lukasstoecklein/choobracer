using UnityEngine;
using System.Collections;
using System.IO.Ports;

public enum Speed {
	InAirWinning, InAirLosing, OnWall, Boost, Crashed, Stopped
}

public class PlayerParentController : MonoBehaviour {
	public bool useArduino = false;
	public GameObject udp;
	public Transform compass;
	public PlayerNum playerNum;
	public GameObject otherPlayer;
	public GameObject sparkController;
	public ParticleSystem[] boostParticleSystemArray;
	public GameObject objHasMusicVals;
	public AudioSource audioSource;
	public GameObject cameraObject;
	public GameObject model;
    public GameObject boostFlare;

	public readonly float strafeSpeed = 2200.0f; 
	public float maxStrafeSpeed = 2200.0f;
	private float moveX = 0.0f;
	private float moveY = 0.0f;
	private float lastMoveX = 0.0f;
	private float lastMoveY = 0.0f;
	private float lastPosZ = 0.0f;
	private bool invertPitch = true;
	
	//target speeds the ship should accelerate to under different conditions
	private Speed curTargetSpeed = Speed.Stopped;
	private Speed lastSpeed = Speed.Stopped;
	private readonly float winningAirSpeed = 120.0f;
	private readonly float losingAirSpeed = 135.0f;
	private readonly float onWallSpeed = 50.0f;
	private readonly float boostSpeed = 375.0f;
	private readonly float crashedSpeed = 20.0f;
	private readonly float stoppedSpeed = 0.0f;
	
	private float timeSpeedLastChanged = 0.0f;
	
	//used to reset the ship on the track if it falls off 
	private readonly float pollTime = 5.0f;	//time between lastPointOnTrack repositioning
	private float lastPolledTime = 0.0f;
	private Vector3 lastPointOnTrack = Vector3.zero;
	
	//arduino
	private string[] bits;
	private SerialPort sp;
	
	//crash control
	private bool bCrashed = false;
	private readonly float crashRecoveryTime = 1.0f;
	private float timeCrashed = 0.0f;
	private float timeSinceLastCrash = 0.0f;
	
	// powerup stuff
	private bool screenFlipped = false;
	public bool boosters = false;
	private bool alreadyBoosting = false;

	public float boostTime = 0;
	private float screenFlipTime = 0;
	public GameObject powerupPlaceholder;
	public GameObject pCamera;
	private float FOV = 62;
	
	//touching wall
	private bool touchingWall = false;
	private float touchingWallHeight = 1.3f;
	private float distFromSurface = 0.0f;
	
	// boost sound
	public AudioClip boost;
	public AudioClip reverse;
	public AudioClip shatter;
	public AudioClip boosterTransform;

	private Vector2 center2 = new Vector3(0, 0);
	
	private bool shipFlightAllowed = false;


	private Vector3 ignoreme = new Vector3(0,0,0);
	
	private const float Duration = 2f;
	private float endTime = -1f;
	// Use this for initialization
	void Start() {
		//open the serialport for arduino if we want to use it.
		if (useArduino) {
			sp = new SerialPort("COM7", 115200);
			sp.Open();
			sp.ReadTimeout = 1; //apparently if this is set higher, unity might freeze while its trying to read
		}
		
		Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, true);
		//	Screen.fullScreen = true;
		
		sparkController.GetComponent<ParticleSystem>().enableEmission = false;
		foreach (ParticleSystem ps in boostParticleSystemArray) {
		//	ps.enableEmission = false;
		}

		lastPosZ = transform.position.z;
	}
	
	
	// Update is called once per frame
	void Update() {
		setInput();
		setTouchingWall();
		constrainToCircle();
		//setRespawnPoint();
		applyEffects(); //sparks etc
		updateAnimations ();
		//respondToBogusData ();
	}
	
	
	//called every fixed framerate frame - rigidbody should be dealt with here
	void FixedUpdate() {
		setShipTargetSpeed();
		moveShip();
		enforceMaxSpeed();
		//resetPositionIfFallenOffTrack();
		setFOV();
	}
	
	void LateUpdate() {
		lastMoveX = moveX;
		lastMoveY = moveY;
		lastPosZ = transform.position.z;
	}
	
	
	void OnCollisionEnter(Collision c) {
		//print (c.gameObject.name);
		if (c.gameObject.name != "lowpolyshell" //have to specify objects that it cant crash head on into, otherwise the crash effect will happen when it e.g. touches the tube side.
		    && c.gameObject.name != "pPipe4"
		    && c.gameObject.name != "pPipe2") {
			bCrashed = true;
			timeCrashed = Time.time;
		}
		if (c.gameObject.name.StartsWith("pCylinder002"))
			audioSource.PlayOneShot(shatter, 1.0f);
	}
	
	
	
	
	private void setInput() {
		//reset values
		moveX = 0;
		moveY = 0;

		if(useArduino){
			//need to make sure the port is definitely open, otherwise catch the exception.
			if (sp.IsOpen) {
				try {
					string lastValidLine = "";
					string lastLine = "";
					
					while ((lastLine = sp.ReadLine()) != null) {
						if (lastLine[0] == '(' && lastLine.Contains("\t")) {
							lastValidLine = lastLine;
							
							bits = lastValidLine.Split('\t');
							
							float pitch;
							float.TryParse(bits[1], out pitch);
							
							float roll;
							float.TryParse(bits[2], out roll);
							
							moveX = pitch * 2.0f;
							moveY = roll * 2.0f;
						}
					}
				}
				catch (System.Exception) { } //just catch it, dont freak out
			}
		}
			
		//keyboard input
		if (playerNum == PlayerNum.P1) {
			moveX += Input.GetAxis("P1Horizontal");
			moveY += Input.GetAxis("P1Vertical");
		}
		else if (playerNum == PlayerNum.P2) {
			moveX += Input.GetAxis("P2Horizontal");
			moveY += Input.GetAxis("P2Vertical");
		}
		
		//udp input
		double[] axis = udp.GetComponent<Mindtuner>().getOrientationVector();		
		if (axis != null) {
			moveX += -(float)axis[1];
			if (invertPitch)
				moveY += -(float)axis[0];
			else
				moveY += (float)axis[0];
		}

		//print(moveX + "\t" + moveY);
	}
	
	
	private void moveShip() {
		//Movement Method 1//////////////////////////
		//		rigidbody.AddForce(transform.right * moveX * strafeSpeed);
		//		rigidbody.AddForce(transform.up * -moveY * strafeSpeed);
		//		rigidbody.velocity = Vector3.Lerp (rigidbody.velocity, new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, curTargetSpeed), Time.deltaTime*acceleration);	//controlling z speed velocity with forces was too unpredictable.z
		////////////////////////////////////////////////////
		
		
		/*//Movement Method 1.5//////////////////////////
		rigidbody.AddForce(transform.right * moveX * strafeSpeed * Time.deltaTime);
		rigidbody.AddForce(transform.up * moveY * strafeSpeed * Time.deltaTime);
		
		//limit movement speed
		if (rigidbody.velocity.magnitude > maxStrafeSpeed)
			rigidbody.velocity = rigidbody.velocity.normalized * maxStrafeSpeed;
		////////////////////////////////////////////////////*/
		
		/*////MOVEMENT METHOD 3//////////////////////
		float power = 0.8f;
		Vector3 forceDir = new Vector3 (tgtParPosX, tgtParPosY, transform.position.z) - transform.position;
		forceDir.Normalize ();
		forceDir = forceDir * power;
		rigidbody.AddForce (forceDir);
		////////////////////////////////////////////////////*/
		
		//if (gameObject.name == "Player1")
		//    print(rigidbody.velocity.z + " " + getSpeedVal(curTargetSpeed));
		
		
		//forward movement
		float spdZ = ((Time.time - timeSpeedLastChanged) < getSpeedChangeTime()) ? (Mathf.Lerp(getSpeedVal(lastSpeed), getSpeedVal(curTargetSpeed), (Time.time - timeSpeedLastChanged) / getSpeedChangeTime())) : getSpeedVal(curTargetSpeed);
		rigidbody.velocity = new Vector3(rigidbody.velocity.x, rigidbody.velocity.y, spdZ);
		
		//Movemement method 2///////////////////
		//X movement0
		float tgtParPosX = 80.0f * moveX;
		float tgtParPosY = 80.0f * moveY;
		float shiftTime = 0.5f;
		transform.position = Vector3.SmoothDamp(transform.position, new Vector3(tgtParPosX, tgtParPosY, transform.position.z), ref ignoreme, shiftTime);
		
		//Y movement
		//transform.position = Vector3.Lerp (transform.position, new Vector3 (transform.position.x, moveY, transform.position.z), shiftSpeed);	
		/////////////////////////////////////////////////
	}
	
	
	//record a valid respawn point periodically
	private void setRespawnPoint() {
		if (touchingWall) {
			if (Time.time - lastPolledTime > pollTime) {
				lastPointOnTrack = (transform.localPosition);
				lastPolledTime = Time.time;
			}
		}
	}
	
	
	private void setShipTargetSpeed() {
		float hoverForce = 1.9F;
		float hoverDamp = 0.1F;
		
		//flight not allowed
		if (!shipFlightAllowed) {
			setTargetSpeed(Speed.Stopped);
		}
		
		//crashed 
		if (bCrashed) {
			bCrashed = false;
			setTargetSpeed(Speed.Crashed);
			return;
		}
		float a = (endTime-Time.time)/Duration;
		//boosters
		if (Time.time < boostTime + 1) {
            setTargetSpeed(Speed.Boost);

            boostFlare.particleSystem.startSize = 2;
        //    if (cameraObject.camera.fieldOfView < 100) cameraObject.camera.fieldOfView++;
           
		}
		else{

            setTargetSpeed(getAirSpeedFromRacePosition());

            boostFlare.particleSystem.startSize = 1;
		//	if(cameraObject.camera.fieldOfView > 80) cameraObject.camera.fieldOfView--;
		}

		
	}
	
	private void setTouchingWall() {
		RaycastHit hit;
		Ray downRay = new Ray(transform.position, compass.position - transform.position);
		if (Physics.Raycast(downRay, out hit)) {
			//if touching tube walls...
			if (hit.distance < touchingWallHeight) {
				distFromSurface = hit.distance;
				touchingWall = true;
				return;
			}
		}
		touchingWall = false;
	}
	
	private void applyEffects() {
		sparkController.GetComponent<ParticleSystem>().enableEmission = touchingWall && (transform.position.z - lastPosZ > 0);  //sparks if touching wall and moving
	}
	
	
	//also sets lastSpeed and timeSpeedLastChanged
	private void setTargetSpeed(Speed s) {
		if (s != curTargetSpeed) {
			lastSpeed = curTargetSpeed;
			curTargetSpeed = s;
			timeSpeedLastChanged = Time.time;
		}
	}
	
	private float getSpeedChangeTime() {
		Speed cur = curTargetSpeed;
		Speed last = lastSpeed;
		float defaultTime = 1.0f;
		
		//flight disallowed
		if (cur == Speed.Stopped)
			return 0.0f;
		
		//crashed
		if (cur == Speed.Crashed)
			return 0.1f;            //instantly go to crashed speed.
		
		//accelerate faster when boosting
		if (cur == Speed.Boost)
			return 3f;
		
		
		//all unspecified cases
		return defaultTime;
	}

    public void boostShipOnce()
    {
        boostTime = Time.time;
    //    boosters = true;

    }
	public void speedBoost() {
		//print ("boosters");

        //foreach (ParticleSystem ps in boostParticleSystemArray) {
        //    ps.enableEmission = true;
        //}

		

		boosters = true;
	//	powerupPlaceholder.GetComponent<PowerupPlaneRotateScript>().renderer.material = powerupPlaceholder.GetComponent<PowerupPlaneRotateScript>().speed;
		audioSource.PlayOneShot(boost, 1.0f);
	}

	public void playBoostTransform(){
		if (!alreadyBoosting) {
			audioSource.PlayOneShot (boosterTransform, 1.0f);
		}

	}

	public void updateAnimations(){

	//	model.transform.GetComponent<Animator>().SetBool("Boosting",boosters);


	}
	
	public void flipScreen() {
		//print ("screen flipping on "+gameObject.name);
		screenFlipped = true;
		screenFlipTime = Time.time;
		powerupPlaceholder.GetComponent<PowerupPlaneRotateScript>().renderer.material = powerupPlaceholder.GetComponent<PowerupPlaneRotateScript>().rotate;
		audioSource.PlayOneShot(reverse, 1.0f);
	}
	
	private void setFOV() {
		float maxSpeed = boostSpeed;
		float speedMod = Mathf.Lerp(0, maxSpeed, rigidbody.velocity.z / maxSpeed);
		//pCamera.camera.fieldOfView = Mathf.Lerp(62f, 110f, rigidbody.velocity.z / maxSpeed);
	}
	
	
	public void stopShip() {
		shipFlightAllowed = false;
		setTargetSpeed(Speed.Stopped);
	}
	
	
	public void startShip() {
		shipFlightAllowed = true;
	}
	
	// increases ship speed slightly if player sucks slightly
	private Speed getAirSpeedFromRacePosition() {
		return (transform.position.z < otherPlayer.transform.position.z - 5) ? Speed.InAirLosing : Speed.InAirWinning;
	}
	
	
	private float getSpeedVal(Speed s) {
		switch (s) {
		case Speed.InAirWinning:
			return winningAirSpeed;
			
		case Speed.InAirLosing:
			return losingAirSpeed;
			
		case Speed.OnWall:
			return onWallSpeed;
			
		case Speed.Boost:
			return boostSpeed;
			
		case Speed.Crashed:
			return crashedSpeed;
			
		case Speed.Stopped:
			return stoppedSpeed;
		}
		
		//if we get here something's wrong, so return a weird value.
		return -1;
	}
	
	
	private void constrainToCircle() {
		float boardMax = 26f;
		if (Vector2.Distance(transform.position, center2) > boardMax) {
			Vector2 newPos = (Vector2)transform.position - center2;
			newPos.Normalize();
			newPos *= boardMax;
			transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
		}
	}
	
	
	private void enforceMaxSpeed() {
		if (rigidbody.velocity.magnitude > boostSpeed)
			rigidbody.velocity = rigidbody.velocity.normalized * boostSpeed;
	}
	
	
	private void resetPositionIfFallenOffTrack() {
		if (Mathf.Abs(transform.position.x * transform.position.y) > 200.0f && transform.position.z < GameObject.Find("FinishTarget").transform.position.z) {
			lastPointOnTrack = new Vector3(lastPointOnTrack.x / 3f, lastPointOnTrack.y / 3f, lastPointOnTrack.z - 20.0f);
			transform.localPosition = lastPointOnTrack;
			
			RaycastHit hit;
			Ray downRay = new Ray(transform.position, compass.position - transform.position);
			while (!(Physics.Raycast(downRay, out hit))) {
				downRay = new Ray(transform.position, compass.position - transform.position);
				transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - 20.0f);
			}
		}
	}
	
	
	private void ignoreBogusData() {
		string datastate = "";
		
		datastate += (Mathf.Abs(moveX - lastMoveX) + " " + Mathf.Abs(moveY - lastMoveY));
		
		float xDiff = Mathf.Abs(moveX - lastMoveX);
		float yDiff = Mathf.Abs(moveY - lastMoveY);
		if (xDiff > 1.0f || xDiff < 0.005f) {
			moveX = lastMoveX;
			datastate += " removed x";
		}
		if (yDiff > 1.0f || yDiff < 0.005f) {
			moveY = lastMoveY;
			datastate += " removed y";
		}
		
		print(datastate);
	}
	
	public void setInvertPitch(bool invert) {
		invertPitch = invert;
	}
}
