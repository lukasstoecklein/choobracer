using UnityEngine;
using System.Collections;
using System.IO.Ports;

public enum PlayerNum{
	P1, P2
}

public class PlayerRotator : MonoBehaviour {

	private Vector2 center2 = new Vector3(0,0);
	public GameObject mesh;
	private float lastY = 0.0f;

	// Use this for initialization
	void Start () {
	}


	// Update is called once per frame
	void Update () {

	}


	//called every fixed framerate frame - rigidbody should only be dealt with here
	void FixedUpdate(){
		ControlShip();
	}

	/// <summary> 
	/// Applies data input to the ship, automatically rotates it, sets its light brightness and range, enforces max speed 
	/// </summary>
	private void ControlShip(){
		rotateFromCenterAndFaceDirectionMoving();
	}


	private void rotateFromCenterAndFaceDirectionMoving(){
		//rotate away from center (0,0,0).//////////////////////////////////////////////////////////
		Vector2 shipPos2 = new Vector2(transform.position.x, transform.position.y);
		Vector2 posFromCenter = shipPos2 - center2;
		float rotSoften = 4.5f; //lower = slower original = 45
		posFromCenter.Normalize();
		float zRot = (Mathf.Atan2(posFromCenter.y, posFromCenter.x)) * Mathf.Rad2Deg + 90.0f;
		//print(zRot);
		
		float xVel = transform.InverseTransformDirection(transform.parent.transform.rigidbody.velocity).x;	//points the nose of the ship in the direction it's going
		Quaternion meshRotTarget = Quaternion.Euler(0, xVel/2, 0);	//converts the z rotation from an euler angle to a quat
		mesh.transform.localRotation = Quaternion.Slerp(mesh.transform.localRotation, meshRotTarget, rotSoften*Time.deltaTime);
		
		Quaternion rotTarget = Quaternion.Euler(0, 0, zRot);	//converts the z rotation from an euler angle to a quat
		transform.rotation = Quaternion.Slerp(transform.rotation, rotTarget, rotSoften*Time.deltaTime);
		///////////////////////////////////////////////////////////////////////////////////////////////////// 
	}
}
