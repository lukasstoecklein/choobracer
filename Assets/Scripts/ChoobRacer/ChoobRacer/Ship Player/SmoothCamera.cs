﻿using UnityEngine;
using System.Collections;

public class SmoothCamera : MonoBehaviour {
	
	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	public Transform target;
	public Transform lookAt;
	private float followDist = 18.0f;
	private float camHeight = 1.9f;
	private Vector2 center2 = new Vector3(0,0);


	// Update is called once per frame
	void FixedUpdate () 
	{
		if (target)
		{
			Vector3 destination = new Vector3(target.transform.position.x/camHeight, target.transform.position.y/camHeight, target.transform.position.z - followDist);
			transform.position = Vector3.SmoothDamp(transform.position, destination, ref velocity, dampTime);

			//lookat
			Quaternion lookrotation = Quaternion.LookRotation(lookAt.position - transform.position);
			//transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 1.0f);

			/*float tgtz = target.transform.eulerAngles.z;
			if(name == "P1 Camera")print (tgtz);
			if(Mathf.Abs(tgtz - transform.eulerAngles.z) < 300.0f)
				transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, tgtz), Time.deltaTime * 8.0f);
			else
				transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, tgtz), Time.deltaTime * 800.0f);*/



			//rotate away from center (0,0,0).//////////////////////////////////////////////////////////
			/*Vector2 camPos2 = new Vector2(transform.position.x, transform.position.y);
			Vector2 posFromCenter = camPos2 - center2;
			float rotSoften = 15.0f;
			posFromCenter.Normalize();
			float zRot = (Mathf.Atan2(posFromCenter.y, posFromCenter.x)) * Mathf.Rad2Deg + 90;
			//print(zRot);
			Quaternion targetQ = Quaternion.Euler(0, 0, zRot);	//converts the z rotation from an euler angle to a quat
			targetQ = targetQ * lookrotation;
			transform.rotation = Quaternion.Slerp(transform.rotation, targetQ, rotSoften*Time.deltaTime);
			*///////////////////////////////////////////////////////////////////////////////////////////////////// 
		}
	}
}