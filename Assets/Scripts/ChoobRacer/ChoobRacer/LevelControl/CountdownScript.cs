﻿using UnityEngine;
using System.Collections;

public class CountdownScript : MonoBehaviour {

	public GameObject p1, p2;
	public GameObject p1Countdown, p2Countdown;
	public GameObject gameScore;
	private float startTime;
	public Texture2D countdown1, countdown2, countdown3, countdownGo;
	public AudioClip three,two,one;
	private bool threePlayed, twoPlayed, onePlayed, goPlayed = false;
	public AudioSource sharedAudioSource;
	private bool started = false;


	private float[] spectrum = new float[256];
	// Use this for initialization
	void Start () {
		startTime = Time.timeSinceLevelLoad;

		p1.GetComponent<PlayerParentController>().stopShip ();
		if(p2) p2.GetComponent<PlayerParentController>().stopShip ();
		p1Countdown.SetActive(true);
		if(p2) p2Countdown.SetActive(true);


	//	sharedAudioSource = GameObject.Find ("AudioSource").audio;
	}
	 
	// Update is called once per frame
	void Update () {

	
		//print (Time.timeSinceLevelLoad);
		
		if((Time.timeSinceLevelLoad >= startTime) && (Time.timeSinceLevelLoad <= startTime + 1)){
			if(!threePlayed){
				sharedAudioSource.PlayOneShot (three, 1.0f);
				threePlayed = true;
			}
			
			p1Countdown.renderer.material.mainTexture = countdown3;
			p1Countdown.transform.localPosition = new Vector3(0,0,13.0f);
			
			if(p2) p2Countdown.renderer.material.mainTexture = countdown3;
			if(p2) p2Countdown.transform.localPosition = new Vector3(0,0,13.0f);
		}
		else if((Time.timeSinceLevelLoad >= startTime) && (Time.timeSinceLevelLoad <= startTime + 2)){
			//print ("checking");
			if(!twoPlayed){
				sharedAudioSource.PlayOneShot (two, 1.0f);
				twoPlayed = true;
			}
			
			p1Countdown.renderer.material.mainTexture = countdown2;
			if(p2) p2Countdown.renderer.material.mainTexture = countdown2;
		}
		else if((Time.timeSinceLevelLoad >= startTime) && (Time.timeSinceLevelLoad <= startTime + 3)){
			if(!onePlayed){
				sharedAudioSource.PlayOneShot (one, 1.0f);
				onePlayed = true;
			}
			
			p1Countdown.renderer.material.mainTexture = countdown1;
			if(p2) p2Countdown.renderer.material.mainTexture = countdown1;
		}
//		else if((Time.timeSinceLevelLoad >= startTime) && (Time.timeSinceLevelLoad <= startTime + 4)){
//			if(!goPlayed){
//				sharedAudioSource.PlayOneShot (go, 1.0f);
//				goPlayed = true;
//			}
//			
//			p1Countdown.renderer.material.mainTexture = countdownGo;
//			if(p2) p2Countdown.renderer.material.mainTexture = countdownGo;
//		}
		
		if(Time.timeSinceLevelLoad >= startTime + 3 && !started){
		//	sharedAudioSource.loop = true;
		//	sharedAudioSource.PlayOneShot(music, 1.0f);
			p1Countdown.SetActive(false);
			if(p2) p2Countdown.SetActive(false);
			p1.GetComponent<PlayerParentController>().startShip ();
			if(p2) p2.GetComponent<PlayerParentController>().startShip ();
			gameScore.GetComponent<GameScore>().startTimer();
			print ("begun");
			started = true;			
		}
	}

	void OnGUI(){
	


//		int i = 1;
//		while (i < 1023) {
//			Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);
//			Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
//			Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);
//			Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.yellow);
//			i++;
//		}
		

	}
}
