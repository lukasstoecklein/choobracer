﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class GameScore : MonoBehaviour {

    public Texture grayBackground;
    public Texture timerIcon;
    public Texture trophyIcon;
    public GUIStyle scoreStyle2;
    public GUIStyle timerIconStyle;
    double score = 0f;
    Score highScore;
    bool begun = false;
    bool newHighScore = false;
    Stopwatch stopwatch;

    // Use this for initialization
    void Start() {
        stopwatch = new Stopwatch();
        //	highScore = GameObject.Find ("ScoreMaster").GetComponent<GameProfileHolder> ().getGameProfile ().getHighScore ();
        //		if ((double)PlayerPrefs.GetFloat ("highscore") <= 1f) {
        //			print ("replacing High score from 0 to 999");
        //			PlayerPrefs.SetFloat ("highscore", 999f);
        //		}
        //		highScore = PlayerPrefs.GetFloat ("highscore");
    }

    // Update is called once per frame
    void Update() {

    }

    void OnGUI() {
        try {
            if (begun) {
                highScore = GameProfileHolder.instance.getGameProfile().getHighScore();
                // you need to start from login scene so that DontDestryOnLoad variabes are even created

            //    GUI.DrawTexture(new Rect(0, 0, 2040, 47), grayBackground);
           //     GUI.DrawTexture(new Rect(Screen.width / 2 - 115, 6, 35, 35), timerIcon);

                // your score
                //	GUI.Label (new Rect (Screen.width / 2 - 34, 1, 50, 50),stopwatch.Elapsed.TotalSeconds.ToString("F1"), scoreStyle);// disable this if lag. just draws shadow.
                GUI.Label(new Rect(Screen.width / 2 - 75, 0, 30, 30), stopwatch.Elapsed.TotalSeconds.ToString("F1"), scoreStyle2);
                // high score
                //	GUI.Label (new Rect (34, 1, 50, 50),highScore.ToString("F1"), scoreStyle);// disable this if lag. just draws shadow
                if (highScore != null) {
                    GUI.Label(new Rect(Screen.width / 2 + 30, 0, 30, 30), highScore.getScore().ToString("F1"), scoreStyle2);
                    GUI.Label(new Rect(Screen.width / 2 + 170, 0, 30, 30), highScore.getName(), scoreStyle2);
                }
          //      GUI.DrawTexture(new Rect(Screen.width / 2 + 120, 6, 35, 35), trophyIcon);
            }
        }
        catch (System.Exception e) {
            GUI.contentColor = Color.red;
            GUI.Label(new Rect(10, 10, 1000, 20), e.ToString());
        }
    }

    public void resumeTimer() {

        if (begun) {
            print("resuming stopwatch");
            stopwatch.Start();
            begun = true;
        }
    }

    public void startTimer() {
        print("starting timer: hi");
        stopwatch.Start();
        begun = true;

    }


    public void stopTimer() {
        print("pausing stopwatch");
        stopwatch.Stop();
    }

    public void finishRace() {
        print("stopping stopwatch");
        score = stopwatch.Elapsed.TotalSeconds;
        stopwatch.Stop();

    }

    public double getTime() {
        return stopwatch.Elapsed.TotalSeconds;
    }

}