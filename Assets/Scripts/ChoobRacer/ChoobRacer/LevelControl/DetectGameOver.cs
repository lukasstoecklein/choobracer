﻿using UnityEngine;
using System.Collections;

public class DetectGameOver : MonoBehaviour {


	public Transform P1;
	public Transform P2;
	public Transform FinishTarget;

	public GameObject gameScore;

	public Texture2D restart,menu;
	public GUIStyle submitStyle;

	bool finished = false;
	bool pause = false;
	bool player1Finished = false;
	bool player2Finished = false;
	bool timerStopped = false;
	string player1Name = "Enter your name";
	string player2Name = "Enter your name";
	double player1Score;
	double player2Score;
	bool player1ScoreSubmitted = false;
	bool player2ScoreSubmitted = false;

	bool invertP1 = true;
	bool invertP2 = true;

	GameProfile currentGameProfile;
	public GUIStyle scoreElementStyle;
	private Vector2 scoreboardScrollPosition;

	// Use this for initialization
	void Start () {
		currentGameProfile = GameProfileHolder.instance.getGameProfile();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && !pause) {
			print ("pause");
			pause = true;		
			gameScore.GetComponent<GameScore>().stopTimer();
			
			Time.timeScale = 0;
		}
		else if (Input.GetKeyDown (KeyCode.Escape) && pause) {
			print ("unpause");
			pause= false;
			gameScore.GetComponent<GameScore>().resumeTimer();
			
			Time.timeScale = 1;
		}
	}

	void OnGUI(){
		if (pause) {
			int btnWid = 250;
			int btnHt = 70;


			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Time.timeScale = 1;
				Application.LoadLevel ("MainGUI");
			}
			invertP1 = GUI.Toggle (new Rect(10, 3*Screen.height/4, 100, 30),invertP1, "Invert Pitch");
			P1.parent.gameObject.GetComponent<PlayerParentController>().setInvertPitch(invertP1);

			invertP2 = GUI.Toggle (new Rect(Screen.width - 110, 3*Screen.height/4, 100, 30),invertP2, "Invert Pitch");
			P2.parent.gameObject.GetComponent<PlayerParentController>().setInvertPitch(invertP2);

		}

	
		if (P1.position.z > FinishTarget.position.z && !player1Finished) {
			player1Finished = true;
			player1Score = gameScore.GetComponent<GameScore>().getTime();
			P1.parent.gameObject.GetComponent<PlayerParentController> ().stopShip ();
			if (!(P2.position.z > FinishTarget.position.z)) { //p1 win
				finished = true;
			}
		}
		if(P2.position.z > FinishTarget.position.z && !player2Finished){
			player2Finished = true;
			player2Score = gameScore.GetComponent<GameScore>().getTime();
			P2.parent.gameObject.GetComponent<PlayerParentController> ().stopShip ();
			if (!(P1.position.z > FinishTarget.position.z)) { //p2 win
				finished = true;
			}
		}
		if(finished){
			if(player1Finished){
				GUI.Label (new Rect(20, Screen.height/8, 150,50), player1Score.ToString("F1"), submitStyle);

				player1Name = GUI.TextArea(new Rect(Screen.width / 6, Screen.height/8, 150,50),player1Name,20);	
				if(!player1ScoreSubmitted){
					if(GUI.Button (new Rect(Screen.width / 2 - (150+ 20),Screen.height/8, 150,50), "Submit score")){
						player1ScoreSubmitted = true;
						Score newScore = new Score(player1Name, player1Score);

						if (currentGameProfile.newScore(newScore)){
							print ("high score yeaaaa");
							GUI.Label(new Rect (Screen.width / 2 - 15, 100, 35, 35),"New High Score!"); 
						}
				}

				}if(player1ScoreSubmitted){
					GUI.Label (new Rect(Screen.width / 2 - (150+ 20),Screen.height/8, 150,50), "Submitted");
				}
			}
			if(player2Finished){
				GUI.Label (new Rect(Screen.width /2 + 20, Screen.height/8, 150,50), player2Score.ToString("F1"), submitStyle);

				player2Name = GUI.TextArea(new Rect((Screen.width / 2)+Screen.width / 6,  Screen.height / 8, 150,50),player2Name,20);
				if(!player2ScoreSubmitted){
					if(GUI.Button (new Rect(Screen.width  - (150 + 20), Screen.height/8, 150,50), "Submit score")){
						player2ScoreSubmitted = true;
						Score newScore = new Score(player2Name, player2Score);
			
						if (currentGameProfile.newScore(newScore)){
							print ("high score yeaaaa");
						}
				}
			
				}
				if(player2ScoreSubmitted){
					GUI.Label (new Rect(Screen.width  - (150 + 20), Screen.height/8, 150,50), "Submitted");
				}
			}

			if(player1Finished && player2Finished && !timerStopped){
				timerStopped = true;
				gameScore.GetComponent<GameScore> ().finishRace ();
			}
			int btnWid = 250;
			int btnHt = 70;
			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Application.LoadLevel ("PlayChoobRacer");
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				gameScore.GetComponent<GameScore> ().finishRace ();
				Application.LoadLevel ("MainGUI");
			}

			GUILayout.BeginArea(new Rect(100 ,Screen.height/8 + 80, 600, 600));
			GUILayout.BeginVertical();
			scoreboardScrollPosition = GUILayout.BeginScrollView (scoreboardScrollPosition,false, true, GUILayout.Width (760), GUILayout.Height (295));

			foreach(Score player in currentGameProfile.getSortedScores()){
				GUILayout.BeginHorizontal();
				GUILayout.Label( player.getScore().ToString("F1") +"            "+ player.getName(), scoreElementStyle );
			//	GUILayout.FlexibleSpace();
			
			//	GUILayout.Label (player.getScore().ToString("F1"), scoreElementStyle);
				
				GUILayout.EndHorizontal();
			}
			GUILayout.EndScrollView();
			GUILayout.EndVertical();
			GUILayout.EndArea();
		}
	}
}
