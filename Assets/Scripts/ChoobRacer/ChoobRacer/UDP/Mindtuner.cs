using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class Mindtuner : MonoBehaviour
{
	public int port = 5555;
	private UdpClient client;
	private IPEndPoint RemoteIpEndPoint;
	private Thread t_udp;
	
	private string lastValidUDP = ":";
	public string allReceivedUDPPackets="";
	
	void Start()
	{
		client = new UdpClient(port);
		RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 5555);
		t_udp = new Thread(new ThreadStart(UDPRead));
		t_udp.Name = "Mindtuner UDP thread";
		t_udp.Start();
	}
	
	public void UDPRead()
	{
		print("listening UDP port " + port);
		while (true)
		{
			try
			{
				byte[] receiveBytes = client.Receive(ref RemoteIpEndPoint);
				string returnData = Encoding.ASCII.GetString(receiveBytes);
				// parsing
				//print(returnData);
				lastValidUDP = returnData;
			}
			catch (Exception e)
			{
				print("Not so good " + e.ToString());
			}
			Thread.Sleep(20);
		}
	}
	
	public string getLastUDP()
	{
		return lastValidUDP;
	}
	
	void Update()
	{
		//if (t_udp != null) 
		//print(t_udp.IsAlive);
		//print (getLastUDP());
		// print (getOrientationVector ());
		
		String dubles = "";
		double[] dubs = getOrientationVector ();
		if (dubs != null)
		{
			foreach (double d in dubs)
			{
				dubles +=  d + "\t";
			}
		}
		//print (dubles);
	}
	
	void OnDisable()
	{
		if (t_udp != null) t_udp.Abort();
		client.Close();
	}
	
	public double[] getOrientationVector()
	{
		double[] orientationVector = new double[3]{0,0,0};
		string latest = getLatestUDPPacket();
		if (String.IsNullOrEmpty(latest))
			return null;
		
		//print ("latest:" + latest);
		string[] stringArray = latest.Split(',');
		String strings = "strngs: ";
		if (stringArray != null)
		{
			foreach (string d in stringArray)
			{
				strings += ", " + d;
			}
		}
		//print (strings);
		if (stringArray.Length < 2) {
			return null;
		}
		else
		{
			double pitch = (-Double.Parse(stringArray[stringArray.Length-2].Trim())) * Math.PI / 180.0; // pitch
			double roll = (-Double.Parse(stringArray[stringArray.Length-1].Trim())) * Math.PI / 180.0; // roll
			
			double total = 0;
		//	print ("pitch : "+pitch);
		//	print ("roll: +"+roll);
	//		if(Math.Abs(pitch) < 0.04d && Math.Abs(roll) < 0.04d){return orientationVector;}
		

			//total = orientationVector[0] = -cos(ypr[2]) * sin(ypr[1]);
			//total = orientationVector[1] = -sin(ypr[2]);
			//total = orientationVector[2] = cos(ypr[2]) * cos(ypr[1]);
			
			orientationVector[0] = -Mathf.Cos((float)roll) * Mathf.Sin((float)pitch);
			orientationVector[1] = -Mathf.Sin((float)roll);
			orientationVector[2] = Mathf.Cos((float)roll) * Mathf.Cos((float)pitch);
			
			total += orientationVector[0];
			total += orientationVector[1];
			total += orientationVector[2];
			
			double scale = 1.0 / total;
			
			orientationVector[0] *= scale;
			orientationVector[1] *= scale;
			orientationVector[2] *= scale;			
		}
		return orientationVector;
	}
	
	public string getLatestUDPPacket()
	{
		allReceivedUDPPackets = "";
		return lastValidUDP;
	}
}