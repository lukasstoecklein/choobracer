﻿using UnityEngine;
using System.Collections;

public class GUIDetectCrash : MonoBehaviour {
	private bool isDead = false;
	public GameObject player;
	private string newName = "";
	bool userHasHitReturn = false;
	bool hasSavedScore = false;
	
	float xAdjust = -25.0f;

	string playerName = "AAA";

	public GameObject menuBG;
	GUIStyle style = new GUIStyle();
	public GUIStyle TextBoxStyle = new GUIStyle();

	// Use this for initialization
	void Start () {
		//PlayerPrefs.DeleteAll();
	}
	
	// Update is called once per frame
	void Update () {
		if(isDead && Input.GetKeyDown("space")){
			Application.LoadLevel(0);
		}
	}

	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.name == "Cube"){
			isDead = true;
		}
	}

	void OnGUI(){
		if(isDead){
			menuBG.SetActive(true);
			showFinalScore();

			if(isHighScore()){
				//say highscore!
				showHighscoreMsg();

				if(!hasSavedScore){
					//get them to enter a name
					if (Event.current.keyCode == KeyCode.Return){
						userHasHitReturn = true;
					}
					else if (!userHasHitReturn){
						showEnterName();

						TextBoxStyle.normal.textColor = Color.white;
						GUI.SetNextControlName("NewName");
						newName = GUI.TextField(new Rect(Screen.width/2-150, 365,350,40), newName, 12, TextBoxStyle);
						GUI.FocusControl("NewName");

						showTopScores();
					}
					else if(userHasHitReturn){
						saveScore();
						hasSavedScore = true;
					}
				}
				else{
					showTopScores();
					showPressSpace();
				}
			}
			else{
				showTopScores();
				showPressSpace();
			}

			Time.timeScale = 0;
		}
		else{
			showPlayingScore();
		}
	}

	public float getThisGameScore(){
        return -1;
	}

	void saveScore(){
		for(int i = 4; i >= 0; i--){
			if(i == 0){
				savePlayer(0,newName,(int)getThisGameScore());
				hasSavedScore = true;
				return;
			}
			else if(getThisGameScore() > getScore(i)){	//shift data in place above down one
				savePlayer (i, getName(i-1), getScore(i-1));
				if(getThisGameScore() <= getScore(i)){	//if the shifted number is bigger than this number, the one above is bigger than our score, so save here.
					savePlayer (i, newName, (int)getThisGameScore());
					hasSavedScore = true;
					return;
				}
			}
		}
	}

	void savePlayer(int i, string name, int score){
		setScore (i,score);
		setName(i, name);
		//print ("new saved: " + getName(i) + " " + getScore(i) + " " + name);
	}

	int getScore(int recordIndex)
	{
		string key = string.Format("RecordScore{0}", recordIndex);
		return PlayerPrefs.GetInt(key, -1);
	}
	
	void setScore(int recordIndex, int score)
	{
		string key = string.Format("RecordScore{0}", recordIndex);
		PlayerPrefs.SetInt(key, score);
	}
	
	string getName(int recordIndex)
	{
		string key = string.Format("RecordName{0}", recordIndex);
		return PlayerPrefs.GetString(key, string.Empty);
	}   
	
	void setName(int recordIndex, string name)
	{
		string key = string.Format("RecordName{0}", recordIndex);
		PlayerPrefs.SetString(key, name);
	}

	bool isHighScore(){
		print (getName(3) + getScore (3));
		return getThisGameScore() > getScore (4);	//score made the top 5
	}
	
	void showTopScores(){
		int yAdjust = 560;
		GUI.color = new Color(255,255,255,1.0f);
		style.fontSize = 45;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleLeft;

		//index
		string numbers = "";
		for(int i = 1; i < 6; i++)
			numbers  = numbers + i + ".\n";
		GUI.Label(new Rect(Screen.width/2 - 300,yAdjust,100,70),numbers,style);

		//score
		style.alignment = TextAnchor.MiddleLeft;
		string names = "";
		for(int i = 0; i < 5; i++)
			names  = names + getName (i) + "\n";
		GUI.Label(new Rect(Screen.width/2 - 200,yAdjust,100,70),names,style);

		//name
		style.alignment = TextAnchor.MiddleRight;
		string scores = "";
		for(int i = 0; i < 5; i++)
			if(getScore (i) >= 0)
				scores  = scores + getScore(i) + "\n";
		else scores = scores + "\n";
		GUI.Label(new Rect(Screen.width/2 + 300,yAdjust,100,70),scores,style);
	}

	void showFinalScore(){
		int yAdjust = 100;
		GUI.color = new Color(255,255,255,1.0f);
		style.fontSize = 101;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label(new Rect(Screen.width/2 + xAdjust,yAdjust,100,70),"Score: " + getThisGameScore(),style);
		
		style.fontSize = 99;
		GUI.color = new Color(255,255,255,1.0f);
		style.normal.textColor = Color.black;
		GUI.Label(new Rect(Screen.width/2 + xAdjust,yAdjust,100,70),"Score: " + getThisGameScore(),style);
	}

	void showPlayingScore(){
		GUI.color = new Color(255,255,255,0.05f);
		float xAdjust = 100.0f;
		GUIStyle style = new GUIStyle();
		
		style.fontSize = 800;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label(new Rect(Screen.width/2 - xAdjust ,Screen.height/2,100,70),"" + getThisGameScore() / 1f, style);
	}
	
	void showHighscoreMsg(){		
		int yAdjust = 80;
		GUI.color = new Color(255,255,255,0.6f);
		style.fontSize = 100;
		style.normal.textColor = Color.red;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label(new Rect(Screen.width/2 + xAdjust + Random.Range(-3,3),yAdjust + 130 + Random.Range(-3,3),100,100),"Highscore!",style);
	}

	void showEnterName(){
		int yAdjust = 80;
		GUI.color = new Color(255,255,255,0.5f);
		style.fontSize = 50;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label(new Rect(Screen.width/2 + xAdjust,yAdjust + 210,100,100),"Enter a name!",style);
	}

	void showPressSpace(){		
		GUI.color = new Color(255,255,255,0.4f);
		style.fontSize = 70;
		style.normal.textColor = Color.black;
		style.alignment = TextAnchor.MiddleCenter;
		GUI.Label(new Rect(Screen.width/2 + xAdjust,Screen.height -100,100,70),"(Press space to restart)",style);
	}
}
