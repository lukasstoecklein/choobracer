﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class SnowGameScore : MonoBehaviour {

    public Texture grayBackground;
    public Texture timerIcon;
    public Texture trophyIcon;
    public GUIStyle scoreStyle2;
    public GUIStyle timerIconStyle;
	public GameObject GUIScore;
    double score = 0f;
    Score highScore;
 
    bool newHighScore = false;
   

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
		score = GUIScore.GetComponent<GUIController> ().getScore ();
    }


    void OnGUI() {
        try {
        
                highScore = GameProfileHolder.instance.getGameProfile().getHighScore();
                // you need to start from login scene so that DontDestryOnLoad variabes are even created

                GUI.DrawTexture(new Rect(0, 0, 2040, 47), grayBackground);
              //  GUI.DrawTexture(new Rect(Screen.width / 2 - 115, 6, 35, 35), timerIcon);

                // your score
                //	GUI.Label (new Rect (Screen.width / 2 - 34, 1, 50, 50),stopwatch.Elapsed.TotalSeconds.ToString("F1"), scoreStyle);// disable this if lag. just draws shadow.
				GUI.Label(new Rect(Screen.width / 2 - 75, 0, 30, 30), score.ToString("F1"), scoreStyle2);
                // high score
                //	GUI.Label (new Rect (34, 1, 50, 50),highScore.ToString("F1"), scoreStyle);// disable this if lag. just draws shadow
                if (highScore != null) {
                    GUI.Label(new Rect(Screen.width / 2 + 30, 0, 30, 30), highScore.getScore().ToString("F1"), scoreStyle2);
                    GUI.Label(new Rect(Screen.width / 2 + 170, 0, 30, 30), highScore.getName(), scoreStyle2);
                }
                GUI.DrawTexture(new Rect(Screen.width / 2 + 120, 6, 35, 35), trophyIcon);
    
        }
        catch (System.Exception e) {
            GUI.contentColor = Color.red;
            GUI.Label(new Rect(10, 10, 1000, 20), e.ToString());
        }
    }

	public double getScore(){
		return score;
	}
	

}