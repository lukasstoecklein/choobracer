﻿using UnityEngine;
using System.Collections;

public class JumpDetector : MonoBehaviour {
    public GameObject player; 
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collision) {

    }
    
    void OnTriggerStay(Collider col) {
		if (col.tag == "Jump") player.GetComponent<SnowPlayerController>().triggerJumpPhysics(col);
		if (col.tag == "FinishArea") player.GetComponent<SnowPlayerController>().triggerFinish();
    }

    void OnTriggerEnter(Collider c) {
		if (c.tag == "Jump") player.GetComponent<SnowPlayerController>().setInJumpCol(true);
    }

    void OnTriggerExit(Collider c) {
        if (c.tag == "Jump") player.GetComponent<SnowPlayerController>().setInJumpCol(false);
    }
}
