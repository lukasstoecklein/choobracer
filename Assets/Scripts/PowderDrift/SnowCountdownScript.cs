﻿using UnityEngine;
using System.Collections;

public class SnowCountdownScript : MonoBehaviour {

	public GameObject p1;
	public GameObject p2;
	public GameObject p1Controller;
	public GameObject p1Countdown, p2Countdown;
	public GameObject gameScore;
	private float startTime;
	public Texture2D countdown1, countdown2, countdown3, countdownGo;
	public AudioClip three,two,one,go, music;
	private bool threePlayed, twoPlayed, onePlayed, goPlayed = false;
	private AudioSource sharedAudioSource;
	private bool started = false;



	// Use this for initialization
	void Start () {
		startTime = Time.time;
	//	p1.transform.rigidbody.constraints = RigidbodyConstraints.FreezeAll;
		if(p2) p2.GetComponent<PlayerParentController>().stopShip ();
		p1Countdown.SetActive(true);
		if(p2) p2Countdown.SetActive(true);
		sharedAudioSource = GameObject.Find ("AudioSource").audio;
	}
	
	// Update is called once per frame
	void Update () {
	

		if((Time.time >= startTime) && (Time.time <= startTime + 1)){
			if(!threePlayed){
				//print ("three");
				sharedAudioSource.PlayOneShot (three, 1.0f);
				threePlayed = true;
			}

			p1Countdown.renderer.material.mainTexture = countdown3;
		//	p1Countdown.transform.localPosition = new Vector3(0,0,13.0f);

			if(p2) p2Countdown.renderer.material.mainTexture = countdown3;
			if(p2) p2Countdown.transform.localPosition = new Vector3(0,0,13.0f);
		}
		else if((Time.time >= startTime) && (Time.time <= startTime + 2)){
			if(!twoPlayed){
				sharedAudioSource.PlayOneShot (two, 1.0f);
				twoPlayed = true;
			}

			p1Countdown.renderer.material.mainTexture = countdown2;
			if(p2) p2Countdown.renderer.material.mainTexture = countdown2;
		}
		else if((Time.time >= startTime) && (Time.time <= startTime + 3)){
			if(!onePlayed){
				sharedAudioSource.PlayOneShot (one, 1.0f);
				onePlayed = true;
			}

			p1Countdown.renderer.material.mainTexture = countdown1;
			if(p2) p2Countdown.renderer.material.mainTexture = countdown1;
		}
		else if((Time.time >= startTime) && (Time.time <= startTime + 4)){
			if(!goPlayed){
				sharedAudioSource.PlayOneShot (go, 1.0f);
				goPlayed = true;
			}

			p1Countdown.renderer.material.mainTexture = countdownGo;
			if(p2) p2Countdown.renderer.material.mainTexture = countdownGo;
		}

		if(Time.time >= startTime + 4 && !started){
			sharedAudioSource.PlayOneShot(music, 1.0f);
			p1Countdown.SetActive(false);
			//p1.transform.rigidbody.constraints = RigidbodyConstraints.None;

			started = true;
			p1Controller.GetComponent<SnowPlayerController>().countdownLocked = false;
		}
	}
}
