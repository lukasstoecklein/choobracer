﻿using UnityEngine;
using System.Collections;

public class TrailController : MonoBehaviour {
    public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<TrailRenderer>().enabled = !player.GetComponent<SnowPlayerController>().getInAir();
    }
}
