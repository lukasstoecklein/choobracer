using UnityEngine;
using System.Collections;
//using System;


public class SnowPlayerController : MonoBehaviour {
    //input
    public GameObject boarderModel;
    public GameObject udp;
    private float inputX = 0.0f;
    private float inputY = 0.0f;

	// other gameobjects called from here
	public GameObject boardTransform;

    //for aligning the snowboarder with the ground nicely
    private const float boardLength = 0.5f;
    private const float heightOfRider = 2.0f;

    //speed and control modifiers
    private readonly float minForwardVelocity = 10f;
    private readonly float maxForwardVelocity = 50f;
    private readonly float slopeAccelerationDivisor = 5f;
    private readonly float maxSlopeTravelAngle = -40f;
    private readonly float turnRate = 40f;
    private readonly float forwardFriction = 0.1f;
    private readonly float verticalNudge = 0.10f;
    private readonly float leanFactor = 0.7f;

    //mountain data
    private float mountainAngle;
    private float groundYHt = 0;
	private float distanceToGround = 0;

	// effects
	private bool restartedSnow = false;

    private Vector3 snowboardPosition;
    private float forwardVelocity = 0f;
    private float heading = 0f;
    private float turnAngle;
    protected Vector3 velocity;

    private float timeLeftGround = 0.0f;
    private float tgtGravityAccel = 0.6f;
    private bool justCameOffJump = false;
    private bool onGround = false;
    private bool stopping = false;
    private bool inJumpCollider = false;

    private float lastYpos = 0.0f;

	private bool hasSetStartTime = false;
	private float startTime = 0;

	public bool countdownLocked = true;

    private float turnAnimSmooth = 0.0f;
	
	private string underBoardTag = "";
	private GameObject curGrindPole;
	private bool isGrinding = false;



	void Start(){
		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		Screen.fullScreen = true;
	}


    void Update()
    {
	
		//print (forwardVelocity);
		if (forwardVelocity > 20.0f ) {
			//print (Mathf.Abs(inputX));
			if(Mathf.Abs(inputX) > 1.2f && distanceToGround < 4.0f){
				boardTransform.particleSystem.enableEmission = true;
			}
			else{
				boardTransform.particleSystem.enableEmission = false;
			}
			//print ("stop snow");

		//	restartedSnow = false;

		}
	
	
		if (countdownLocked)
			return;

		if (!hasSetStartTime) {
			startTime = Time.time;
			hasSetStartTime = true;
		}

        setInputXY();
        setMountainInfo();


        //////////////turn player///////////////////////////////////////////////
        if (!justCameOffJump || onGround) {    //disable normal movement if in air
            turnAngle = inputY * turnRate;

			if(!stopping && underBoardTag != "Grind")
				heading = heading + turnAngle * Time.deltaTime;			
			
			if(underBoardTag == "Grind"){
				isGrinding = true;
				//rotate to head towards grind head
				foreach (Transform child in curGrindPole.transform){
					if(child.name == "Grind Head"){
						//rotate to face end of  grind
						//float angle = 0.0F;
						//Vector3 axis = Vector3.zero;
						//Quaternion targetRotation = Quaternion.LookRotation (transform.position - child.position);
						//targetRotation.ToAngleAxis(out angle, out axis);
						//print (heading + " " + angle + " " + transform.localEulerAngles.y);
						//transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, Time.deltaTime*0.9f);	
					}
				}
			}
			else
				isGrinding = false;
        }
		else{
			if(turnAngle != 0) turnAngle -= turnAngle / Mathf.Abs(turnAngle);  //move to zero
			isGrinding = false;
		}
        float tiltVertSmooth = (getInAir()) ? (Mathf.Lerp(0.1f, 0.01f, Time.deltaTime * 40)) : (0.1f);       //sets tilt smoothing for in air and on ground


        // calculate rotation based on slope of mountain and snowboard heading and turn amount
        Quaternion pitch = Quaternion.AngleAxis(mountainAngle, Vector3.left);
        Quaternion yaw = Quaternion.AngleAxis(heading, Vector3.up);
        Quaternion roll = stopping ? Quaternion.AngleAxis(0, Vector3.forward) : Quaternion.AngleAxis(turnAngle * leanFactor, Vector3.forward);
		/*if(underBoardTag != "Grind")*/transform.rotation = Quaternion.Slerp(transform.rotation, yaw * pitch * roll, tiltVertSmooth);
        ////////////////////////////////////////////////////////////////////////


        //calculate forward velocity based on slope of mountain
        if(!stopping){
            float accelerationFromSlope = mountainAngle / slopeAccelerationDivisor;
            if (mountainAngle < maxSlopeTravelAngle) forwardVelocity = Mathf.Lerp(forwardVelocity, 0, Time.deltaTime);
            forwardVelocity += accelerationFromSlope * Time.deltaTime;
            forwardVelocity = Mathf.Clamp(forwardVelocity, minForwardVelocity, maxForwardVelocity);
            forwardVelocity -= forwardFriction * Time.deltaTime;
        }
        else 
            forwardVelocity = Mathf.Lerp(forwardVelocity, 0, Time.deltaTime*2.5f);   //slow to a stop

        // update position based on forward velocity
        Vector3 pos = transform.position;                
        Vector3 forwardVelocity2 = Vector3.forward * -forwardVelocity * Time.deltaTime;
		Vector3 headingOffset = yaw * forwardVelocity2;
        pos += headingOffset;        

        //apply gravity 
        pos.y = updateByGravity(pos.y);

        //apply all transformations
        transform.position = pos;

        //add airtime to score
        if (justCameOffJump && !onGround) {
            GetComponent<GUIController>().addToScore(Time.deltaTime);
        }

        //check for ddr solvingness and update animations
        updateAnimations();


    }

    private void updateAnimations() {
        //turning animation data
		turnAnimSmooth = Mathf.Lerp (turnAnimSmooth, inputY, Time.deltaTime * 6);
		boarderModel.GetComponent<Animator>().SetFloat("Direction", turnAnimSmooth);

        //set when in air after jump
        if (!getInAir())
            boarderModel.GetComponent<Animator>().SetTrigger("Jumping");

        //move to falling position in air
        boarderModel.GetComponent<Animator>().SetBool("InAir", getInAir());

		//grinding
		boarderModel.GetComponent<Animator> ().SetBool ("Grinding", isGrinding);
        
        //ddr animations
        if (checkDDRSuccess()) {
            boarderModel.GetComponent<Animator>().SetInteger("Trick", getTrickInt(GetComponent<GUIController>().getCurDDRMove()));
            boarderModel.GetComponent<Animator>().SetTrigger("Jumping");
            GetComponent<GUIController>().advanceDDRStream();
        }

        //set landing triggers if necessary
        if (!stopping && closeToGround() && !onGround) {
            boarderModel.GetComponent<Animator>().SetInteger("Trick", 0);
            boarderModel.GetComponent<Animator>().SetTrigger("Landing");
        }

        //
        if (onGround)
            boarderModel.GetComponent<Animator>().SetInteger("Trick", 0);

        //check for enter finish area
        if (stopping) {
            boarderModel.GetComponent<Animator>().SetBool("Stopping", true);
            boarderModel.GetComponent<Animator>().SetInteger("Trick", 0);
        }
    }

    private int getTrickInt(DDRMove d) {
        switch (d) {
            case DDRMove.U:
                return 2;
            case DDRMove.D:
                return 1;
            case DDRMove.L:
                return 3;
            case DDRMove.R:
                return 4;
            default:
                return 0;
        }
    }

    private bool closeToGround() {
        float heightThatCountsAsClose = 2.5f;
        return transform.position.y - groundYHt < heightThatCountsAsClose;
    }

    private void setInputXY() {
		inputX = 0;
		inputY = 0;

		//udp input
		double[] axis = udp.GetComponent<Mindtuner>().getOrientationVector();
		if(axis != null)
		{
			inputX = (float)axis[1]*4.0f;
			inputY = -(float)axis[1]*4.0f;
			inputX = Mathf.Clamp(inputX, -2.0f, 2.0f);
			inputY = Mathf.Clamp(inputY, -2.0f, 2.0f);
		}
        
		//keyboard input
	    inputX += Input.GetAxis("Vertical");
	    inputY += Input.GetAxis("Horizontal");
        
		bool hitBlock = false;

		Vector3 forwardVector = -transform.forward;
		Debug.DrawRay (transform.position + new Vector3(0,1,0), forwardVector * 5, Color.green);
		RaycastHit hit;
		Physics.Raycast (transform.position + new Vector3(0,1,0), forwardVector * 5, out hit);
		if (hit.collider != null) {
			if (hit.distance < 10f && (hit.collider.name.StartsWith("polySurface") || hit.collider.name == "Block") && hit.collider.name != "polySurface6"){
				hitBlock = true;
			}
		}

		Debug.DrawRay (transform.position + new Vector3(2,1,0), forwardVector * 5, Color.green);
		Physics.Raycast (transform.position + new Vector3(2,1,0), forwardVector * 5, out hit);
		if (hit.collider != null) {
			if (hit.distance < 10f && (hit.collider.name.StartsWith("polySurface") || hit.collider.name == "Block") && hit.collider.name != "polySurface6"){
				hitBlock = true;
			}
		}

		Debug.DrawRay (transform.position + new Vector3(-2,1,0), forwardVector * 5, Color.green);
		Physics.Raycast (transform.position + new Vector3(-2,1,0), forwardVector * 5, out hit);
		if (hit.collider != null) {
			if (hit.distance < 10f && (hit.collider.name.StartsWith("polySurface") || hit.collider.name == "Block") && hit.collider.name != "polySurface6"){
				hitBlock = true;
			}
		}

		if (hitBlock) {
			Vector3 reflected = Vector3.Reflect(transform.position, hit.normal);
			Vector3 normalizedReal = transform.forward.normalized;
			reflected = reflected.normalized;
		
			float dotProduct = Vector3.Dot (normalizedReal,reflected);
			forwardVelocity = Mathf.Abs(dotProduct + 0.2f) * forwardVelocity;
			print(dotProduct);
			//transform.rotation = Quaternion.Euler(reflected.x,reflected.y,reflected.z);
							//	print (hit.normal);
							//		print ("rocks ahoi");
			if(hit.normal.x > 0){
				//print ("<<<<<<<<<<");
				inputY = -2f;
			
			}
			else{
				//print (">>>>>>>>>>");
				inputY = 2f;
			}
		}

		//inputY = 2f; 
	   //print (inputY);


    }


    //records the mountain's angle and height below the player
    private void setMountainInfo() {
        Quaternion myRot = this.transform.rotation;
        Quaternion currentHeadingRotation = Quaternion.AngleAxis(myRot.eulerAngles.y, Vector3.up);

        Vector3 downRay = Vector3.down; //transform.TransformDirection(Vector3.down);
        Vector3 posFrontOfSnowboard = transform.position + (currentHeadingRotation * new Vector3(0.0f, heightOfRider, -(boardLength / 2.0f)));
        Vector3 posBackOfSnowboard = transform.position + (currentHeadingRotation * new Vector3(0.0f, heightOfRider, (boardLength / 2.0f)));
        Vector3 posMiddleOfSnowboard = transform.position + new Vector3(0.0f, heightOfRider, 0.0f);

        RaycastHit rcDownFront;
        Physics.Raycast(posFrontOfSnowboard, downRay, out rcDownFront);
        RaycastHit rsDownBack;
        Physics.Raycast(posBackOfSnowboard, downRay, out rsDownBack);
        RaycastHit rcDownMiddle;
		Physics.Raycast(posMiddleOfSnowboard, downRay, out rcDownMiddle);

		distanceToGround = rcDownMiddle.distance;
		//check for on grind pole
		if (Physics.Raycast (posMiddleOfSnowboard, downRay, out rcDownMiddle)) {
			underBoardTag = rcDownMiddle.transform.gameObject.tag;
			if(underBoardTag == "Grind")
				curGrindPole = rcDownMiddle.transform.gameObject;
		}

        float mountainSlope = (rsDownBack.point.y - rcDownFront.point.y) / boardLength;
        mountainAngle = Mathf.Atan(mountainSlope) * Mathf.Rad2Deg;
        groundYHt = rcDownMiddle.point.y + verticalNudge;
        //print("Mount Angle: " + mountainAngle + " MaxTravelSlope: " + maxSlopeTravelAngle);
    }


    private float updateByGravity(float posY)
    {
        onGround = posY <= groundYHt;
        float playerGravity = 0.0f;
        float descentSpeed = 0.0f;

        float timeSwitchSpeeds = 0.5f;
        descentSpeed = Mathf.Lerp(descentSpeed, tgtGravityAccel, ((Time.time - startTime) - timeLeftGround) / timeSwitchSpeeds);

        float yvel = 0.0f;
        float smoothTime = 0.07F;
        if (onGround) {
            timeLeftGround = Mathf.SmoothDamp(timeLeftGround, (Time.time - startTime), ref yvel, smoothTime);
            tgtGravityAccel = 0.9f;
        }
        playerGravity = -descentSpeed * ((Time.time - startTime) - timeLeftGround);

		//print (Time.time - startTime);

        //stop the player going under the ground        
        if (groundYHt == 0.1f) {
            posY = lastYpos + 1f;
        }
        else if (onGround) {
            float timeFloatOnSnow = 0.2f;
            posY = Mathf.Lerp(posY, groundYHt, ((Time.time - startTime) - timeLeftGround) / timeFloatOnSnow);
            justCameOffJump = false;
        }

        lastYpos = posY;

        return posY += playerGravity;
    }

	private bool checkDDRSuccess(){
		//print(inputX + " " + inputY);
		if (getInAir()) { 
			switch (GetComponent<GUIController>().getCurDDRMove()){
			case DDRMove.U:
				return inputX > 0.1f && Mathf.Abs(inputY) < 0.5f;
			case DDRMove.D:
				return inputX < -0.1f && Mathf.Abs(inputY) < 0.5f;
			case DDRMove.L:
				return Mathf.Abs(inputX) < 0.5f && inputY < -0.1f;
			case DDRMove.R:
				return Mathf.Abs(inputX) < 0.5f && inputY > 0.1f;
			}
		}
		return false;
	}


    public bool getInAir() {
        return !onGround && justCameOffJump;
    }
    public bool getOnGround() {
        return onGround;
    }

    public bool getStopping() {
        return stopping;
    }


    //triggered by the jump detector in the model for the body.
    public void triggerJumpPhysics(Collider col) {
        if (col.transform.tag == "Jump") {
            tgtGravityAccel = 0.3f;
            justCameOffJump = true;
        }
    }

    public void triggerFinish() {
        stopping = true;
    }

    public void setInJumpCol(bool b) {
        inJumpCollider = b;
    }
}