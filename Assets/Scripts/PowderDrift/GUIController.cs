﻿using UnityEngine;
using System.Collections;


public enum DDRMove {
    U,D,L,R
}


public class GUIController : MonoBehaviour {
    //all gui textures
    public Texture bg;
    public Texture moveBase;
    public Texture moveShadow;
    public Texture leftArrow;
    public Texture rightArrow;
    public Texture upArrow;
    public Texture downArrow;

    //text
    public GUIStyle scoreStyle;
    public GUIStyle timerStyle;
    public GUIStyle btnStyle;

    public GameObject ddrTarget;

    //gui matrix stuff
    //private Vector3 scale; 
    //private float originalWidth = 1880f;  // the resolution that the gui looks best at - choose something average, or the target monitor res
    //private float originalHeight = 1000; 

    private float score = 0f;
    private float timer = 0f;

    private DDRMove[] moves = new DDRMove[100];
    private int nextMovePos = 0;

    //for shifting the move targets down
    private bool moveHit = false;
    private float moveHitTime = 0.0f;
    private readonly float moveHitTimeToReturn = 0.1f;

    //fade
    private float guiAlpha = 0.1f;
    private float tgtGuiAlpha = 0.1f;


	// Use this for initialization
	void Start () {
        //set up moves
        DDRMove lastAdded = getRandomMove(); 
        for (int i = 0; i < moves.Length; i++) {
            DDRMove next;
            do {next = getRandomMove();}
            while (next == lastAdded);  //never have consecutive moves be the same
            moves[i] = next;
            lastAdded = next;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!GetComponent<SnowPlayerController>().getStopping()) timer += Time.deltaTime;
	}

	public float getScore(){
		return score;
	}

    private bool getInAir(){
        return this.GetComponent<SnowPlayerController>().getInAir();
    }

    private DDRMove getRandomMove() {
        int j = Random.Range(0, System.Enum.GetNames(typeof(DDRMove)).Length);
        System.Array A = System.Enum.GetValues(typeof(DDRMove));
        return (DDRMove)A.GetValue(UnityEngine.Random.Range(0, A.Length));
    }

    void OnGUI() {
        /*scale.x = Screen.width / originalWidth; // calculate x scale
        scale.y = Screen.height / originalHeight; // calculate y scale
        scale.z = 1;
        var svMat = GUI.matrix; // save current matrix
        // substitute matrix - scale is altered from standard
        GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, scale);
        */// draw GUI controls here:
        ///////////////////////////////////////////////

        setGuiFade();
        drawDDRStuff();
        drawScoreAndTimer();

        if (GetComponent<SnowPlayerController>().getStopping()) {
            showFinishMessage();
        }

        ///////////////////////////////////////////////
        // restore matrix before returning
        //GUI.matrix = svMat; // restore matrix
    }

    private void showFinishMessage() {
        int btnWd = 700;
        int btnHt = 100;
        if (GUI.Button(new Rect(Screen.width / 2 - btnWd / 2, Screen.height / 2 - btnHt / 3, btnWd, btnHt), "Congrats, you got " + (score * 10).ToString("n0") + "! \nClick to Exit.", btnStyle)) {
            Application.Quit();
        }
    }

    public void setGuiFade() {
        float maxFade = 0.9f;
        float minFade = 0.0f;
        float fadeVal = 0.05f;  //speed of fade

        Color c = GUI.color;
        c.a = guiAlpha;
        GUI.color = c;

        if (getInAir()) { tgtGuiAlpha = maxFade; }
        else { tgtGuiAlpha = minFade; }

        if (guiAlpha < tgtGuiAlpha) guiAlpha += fadeVal;
        if (guiAlpha > tgtGuiAlpha) guiAlpha -= fadeVal;
    }

    private void drawDDRStuff() {
        //int x = (int)(3 * Screen.width / 4);
        //int yBase = 600;
        //int y = yBase;
        int popDist = 100;

        Vector3 screenPos = Camera.main.WorldToScreenPoint(ddrTarget.transform.position);
        float x = screenPos.x;
        float y = screenPos.y;
        float xBase = screenPos.x;
        float yBase = screenPos.y;


        DDRMove n = moves[nextMovePos];
        float xMod = 0;
        float yMod = 0;
        int dist = 180;
        switch (n) {
            case DDRMove.U:
                xMod = 0;
                yMod = -dist;
                break;
            case DDRMove.D:
                xMod = 0;
                yMod = dist;
                break;
            case DDRMove.L:
                xMod = -dist;
                yMod = 0;
                break;
            case DDRMove.R:
                xMod = dist;
                yMod = 0;
                break;
        }

        if (moveHit) {
            x = Mathf.Lerp(xBase - xMod/2, xBase, (Time.time - moveHitTime) / moveHitTimeToReturn);
            y = Mathf.Lerp(yBase - yMod/2, yBase, (Time.time - moveHitTime) / moveHitTimeToReturn);
        }

        float scale = 120;
        GUI.DrawTexture(new Rect(x + xMod, y + yMod, scale, scale), moveBase, ScaleMode.ScaleToFit, true);            //base
        GUI.DrawTexture(new Rect(x + xMod, y + yMod, scale, scale), getTexFromDDR(n), ScaleMode.ScaleToFit, true);    //arrow

        if (y == yBase && (n == DDRMove.U || n == DDRMove.D) || x == xBase && (n == DDRMove.L || n == DDRMove.R))
            moveHit = false;
    }

    private Texture getTexFromDDR(DDRMove d) {
        switch (d){
            case DDRMove.U:
                return upArrow;
            case DDRMove.D:
                return downArrow;
            case DDRMove.L:
                return leftArrow;
            case DDRMove.R:
                return rightArrow;
            default:
                return upArrow;
        }
    }

    public DDRMove getCurDDRMove(){
        if(nextMovePos < moves.Length)
            return moves[nextMovePos];
        return DDRMove.D;   //should probably be changed
    }

    public void advanceDDRStream() {
        nextMovePos++;
        moveHit = true;
        moveHitTime = Time.time;
        score += 10;
    }

    private void drawScoreAndTimer() {
        Color c = GUI.color;
        c.a = 1.0f;
        GUI.color = c;

     //   GUI.Label(new Rect(10, 40, 100, 20), "Score: " + (score * 10).ToString("n0"), scoreStyle);
        GUI.Label(new Rect(10, Screen.height - 90, 100, 20), "Timer: " + timer.ToString("n1"), timerStyle);
    }
    
    public void addToScore(float f){
        score += f;
    }
}
