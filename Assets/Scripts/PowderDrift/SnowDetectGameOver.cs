﻿using UnityEngine;
using System.Collections;

public class SnowDetectGameOver : MonoBehaviour {


	public Transform P1;
//	public Transform P2;
	public Transform FinishTarget;

	public GameObject gameScore;

	public Texture2D restart,menu;
	public GUIStyle submitStyle;

	bool finished = false;
	bool pause = false;
	bool player1Finished = false;
	bool player2Finished = false;
	bool timerStopped = false;
	string player1Name = "Enter your name";
	string player2Name = "Enter your name";
	double player1Score;
	double player2Score;
	bool player1ScoreSubmitted = false;
	bool player2ScoreSubmitted = false;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && !pause) {
			print ("pause");
			pause = true;				
			Time.timeScale = 0;
		}
		else if (Input.GetKeyDown (KeyCode.Escape) && pause) {
			print ("unpause");
			pause= false;		
			Time.timeScale = 1;
		}
	}

	void OnGUI(){
		if (pause) {
			int btnWid = 250;
			int btnHt = 70;
			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				Time.timeScale = 1;
				Application.LoadLevel ("MainGUI");
			}
		}

	
		if (transform.gameObject.GetComponent<SnowPlayerController>().getStopping()) {
			player1Finished = true;
			player1Score = gameScore.GetComponent<SnowGameScore>().getScore();
			finished = true;
			//P1.parent.gameObject.GetComponent<PlayerParentController> ().stopShip ();
//			if (!(P2.position.z > FinishTarget.position.z)) { //p1 win
//				finished = true;
//			}
		}
//		if(P2.position.z > FinishTarget.position.z && !player2Finished){
//			player2Finished = true;
//			player2Score = gameScore.GetComponent<GameScore>().getTime();
//			P2.parent.gameObject.GetComponent<PlayerParentController> ().stopShip ();
//			if (!(P1.position.z > FinishTarget.position.z)) { //p2 win
//				finished = true;
//			}
//		}
		if(finished){
			if(player1Finished){
				GUI.Label (new Rect(20, Screen.height/8, 150,50), player1Score.ToString("F1"), submitStyle);

				player1Name = GUI.TextArea(new Rect(Screen.width / 6, Screen.height/8, 150,50),player1Name,20);	
				if(!player1ScoreSubmitted){
					if(GUI.Button (new Rect(Screen.width / 2 - (150+ 20),Screen.height/8, 150,50), "Submit score")){
						player1ScoreSubmitted = true;
						Score newScore = new Score(player1Name, player1Score);
						GameProfile currentGameProfile = GameProfileHolder.instance.getGameProfile();
						if (currentGameProfile.newScore(newScore)){
							print ("high score yeaaaa");
							GUI.Label(new Rect (Screen.width / 2 - 15, 100, 35, 35),"New High Score!"); 
						}
				}

				}if(player1ScoreSubmitted){
					GUI.Label (new Rect(Screen.width / 2 - (150+ 20),Screen.height/8, 150,50), "Submitted");
				}
			}
//			if(player2Finished){
//				GUI.Label (new Rect(Screen.width /2 + 20, Screen.height/8, 150,50), player2Score.ToString("F1"), submitStyle);
//
//				player2Name = GUI.TextArea(new Rect((Screen.width / 2)+Screen.width / 6,  Screen.height / 8, 150,50),player2Name,20);
//				if(!player2ScoreSubmitted){
//					if(GUI.Button (new Rect(Screen.width  - (150 + 20), Screen.height/8, 150,50), "Submit score")){
//						player2ScoreSubmitted = true;
//						Score newScore = new Score(player2Name, player2Score);
//						GameProfile currentGameProfile = GameObject.Find ("ScoreMaster").GetComponent<GameProfileHolder>().getGameProfile();
//						if (currentGameProfile.newScore(newScore)){
//							print ("high score yeaaaa");
//						}
//				}
//			
//				}
//				if(player2ScoreSubmitted){
//					GUI.Label (new Rect(Screen.width  - (150 + 20), Screen.height/8, 150,50), "Submitted");
//				}
//			}

			if(player1Finished && player2Finished && !timerStopped){
				timerStopped = true;
				gameScore.GetComponent<GameScore> ().finishRace ();
			}
			int btnWid = 250;
			int btnHt = 70;
			if(GUI.Button (new Rect(Screen.width/2 - btnWid ,3*Screen.height/4,btnWid,btnHt), restart)){
				Time.timeScale = 1;
				Application.LoadLevel (Application.loadedLevel);
			}
			if(GUI.Button (new Rect(Screen.width/2 + 3 ,3*Screen.height/4,btnWid,btnHt), menu)){
				Time.timeScale = 1;
				Application.LoadLevel ("MainGUI");
			}
		}
	}
}
