using UnityEngine;
using System.Collections;

public class LoginGOD : MonoBehaviour
{
		private string consoleString = "";
		public static string emailAddressString;
		public static string passwordString;
		public string emailAddressLabel = "Email Address";
		public string passwordLabel = "Password";
		public bool passwordtexterased = false;
		public bool emailtexterased = false;
		public GUIStyle loginGUIStyle;
		public GUIStyle forgotPasswordGUIStyle;
		public GUIStyle labelStyle;
        public Texture loginBackground;
		WWW w;

		// login 
		private bool loggingIn = false;
		private bool freshAttempt = true;
	
		// Use this for initialization
		void Start ()
		{
				//	Application.LoadLevel ("MainGUI");
				emailAddressString = "";
				passwordString = "";

				//Screen.SetResolution(1040, 700, false);
		}
	
		// Update is called once per frame
		void Update ()
		{
            Event e = Event.current;
            if (e.keyCode == KeyCode.Return)
            {


                if (!loggingIn)
                {
                    Login();

                    loggingIn = true;

                }

            }
            else if (e.keyCode == KeyCode.Tab)
            {


                if (GUI.GetNameOfFocusedControl() == "emailbox")
                {
                    freshAttempt = false;
                }

            }
		}
	
		void OnGUI ()
		{
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), loginBackground);
            GUILayout.BeginArea(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 4, Screen.height / 4));

            GUILayout.EndArea();

				
		
				GUI.SetNextControlName ("emailbox");
				if (freshAttempt) {
						GUI.FocusControl ("emailbox");	
				}
				emailAddressString = GUI.TextField (new Rect (Screen.width / 2 - 25, Screen.height / 2, 200, 20), emailAddressString, 40);
				GUI.SetNextControlName ("passwordbox");
				passwordString = GUI.PasswordField (new Rect (Screen.width / 2f - 25, Screen.height / 1.8f, 200, 20), passwordString, "*" [0], 40);
				GUI.Label (new Rect (Screen.width / 2.7f, Screen.height / 2, 200, 20), emailAddressLabel, labelStyle);
				GUI.Label (new Rect (Screen.width / 2.7f, Screen.height / 1.8f, 200, 20), passwordLabel, labelStyle);
				GUI.TextArea (new Rect (10, 10, Screen.width - 20, 40), consoleString);
		
				if (GUI.GetNameOfFocusedControl () == "passwordbox") { 
						if (passwordtexterased == false) { 
								passwordString = ""; 
								passwordtexterased = true; 
								freshAttempt = false;
						}  
				}
				if (GUI.GetNameOfFocusedControl () != "passwordbox" && passwordString == "") { 
						if (passwordtexterased == true) { 
								passwordString = ""; 
								passwordtexterased = false; 
						}  
				}
		
				if (GUI.GetNameOfFocusedControl () == "emailbox") { 
						if (emailtexterased == false) { 
								emailAddressString = ""; 
								emailtexterased = true; 
						}  
				}
				if (GUI.GetNameOfFocusedControl () != "emailbox" && emailAddressString == "") { 
						if (emailtexterased == true) { 
								emailAddressString = ""; 
								emailtexterased = false; 
						}  
				}
		
		
				if (GUI.Button (new Rect (Screen.width / 1.7f - 17, Screen.height / 1.6f, 100, 25), "Login", loginGUIStyle)) {
						Debug.Log ("Logging in");
						Login ();
			
				}
		
		
				if (GUI.Button (new Rect (Screen.width / 2.7f, Screen.height / 1.6f, 185, 25), "Forgot Password", forgotPasswordGUIStyle)) {
						Debug.Log ("Retrieving password");
						ForgotPassword ();
				}
		
//				if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.Return) {
//						Application.LoadLevel ("MainGUI");
//				}

				
		
		
		}

		private void ForgotPassword ()
		{
				WWWForm form = new WWWForm ();
				form.AddField ("form_email", emailAddressString);
				//form.AddField("form_password",passwordString);
				w = new WWW ("hiddenexpo.nfshost.com/unity/userforgotpassword.php", form);
				StartCoroutine (WaitForWWWForgotPassword ());
		}

		private void Login ()
		{
				if (emailAddressString == "" && passwordString == "") {
						Application.LoadLevel ("MainGUI");
				}
				WWWForm form = new WWWForm ();
				form.AddField ("form_email", emailAddressString);
				form.AddField ("form_password", passwordString);
				w = new WWW ("hiddenexpo.nfshost.com/unity/userlogin.php", form);
				StartCoroutine (WaitForWWWLogin ());
		
		
		}
	
		public IEnumerator WaitForWWWLogin ()
		{
			
				yield return w;

				if (w.error != null) {
						print (w.error);
				} else {
						print ("no error");
				}
				if (w.text == "login_success") {
						PlayerPrefs.SetString ("emailaddress", emailAddressString);
						PlayerPrefs.SetString ("password", passwordString);
						Application.LoadLevel ("MainGUI");
				} else if (w.text == "") {
						consoleString = "Wrong email or password";
						loggingIn = false;
				}

		
		}

		public IEnumerator WaitForWWWForgotPassword ()
		{
				yield return w;
				if (w.error != null) {
						print (w.error);
				} else {
						print ("no error");
				}
		
				if (w.text == "forgotpassword_success") {
						consoleString = "Password sent to " + emailAddressString;
				} else {
						consoleString = "Couldn't find email address";
				}
		
		}
}

