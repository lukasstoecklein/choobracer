﻿using UnityEngine;
using System.Collections;

public class CameraPivot : MonoBehaviour {

    public GameObject lookAt;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.LookAt(lookAt.transform);
        Vector3 mousePosition = Input.mousePosition;
        float standardisedX = (mousePosition.x / Screen.width - 0.5f);
        float standardisedY = (mousePosition.y / Screen.height - 0.5f);


        this.transform.position = new Vector3(standardisedX, standardisedY, 0);
	}
}
